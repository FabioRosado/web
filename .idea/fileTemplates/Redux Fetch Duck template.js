import { errorArray } from 'services/utils/error';

const initialState = {
    isLoading: false,
    failed: false,
    // add stuff here
    errorMessages: null,
}

export const types = {
    ${FetchActionPrefix}_FETCH_REQUESTED: "${FetchActionPrefix}_FETCH_REQUESTED",
    ${FetchActionPrefix}_FETCH_SUCCESS: "${FetchActionPrefix}_FETCH_SUCCESS",
    ${FetchActionPrefix}_FETCH_FAILED: "${FetchActionPrefix}_FETCH_FAILED",
}

// rename to name
export const ${NAME}Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.${FetchActionPrefix}_FETCH_REQUESTED:
            return {
                ...state,
                isLoading: true,
                failed: false,
            }

        case types.${FetchActionPrefix}_FETCH_SUCCESS:
            return {
                ...state,
                isLoading: false,
                failed: false,
            }

        case types.${FetchActionPrefix}_FETCH_FAILED:
            return {
                ...state,
                failed: true,
                isLoading: false,
                errorMessages: action.errorMessages
            }

        default:
            return state
    }
}

export const actions = {
    fetch${MayusFileName}: () => {
        return {
            type: ${FetchActionPrefix}_FETCH_REQUESTED,
        }
    },
    
    fetch${MayusFileName}Success: ( data ) => {
        return {
            type: ${FetchActionPrefix}_FETCH_SUCCESS,
            // do stuff
        }
    },
    
    fetch${MayusFileName}Failed: (errorMessages = ["Failed to fetch data."]) => {
        return {
            type: ${FetchActionPrefix}_FETCH_FAILED,
            errorMessages: errorArray(errorMessages),
        }
    }
}