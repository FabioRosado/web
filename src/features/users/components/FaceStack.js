import React from 'react';
import styled from 'styled-components';
import { Avatar } from "features/users";

const FaceStack = styled.ul`
    list-style: none;
    line-height: 0px;

    li {
        display: inline-block;
    }

    li:not(:first-child) {
        margin-left: -11px;
    }

    li img {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        border: 2px solid #ffffff;
    }
`

export default ({ users, is=24, limit=5 }) => (
    <FaceStack>
        {users.slice(0, limit).map(
            u => <li><Avatar is={is ? is : 24} user={u} withAura={false} /></li>
        )}
    </FaceStack>
)