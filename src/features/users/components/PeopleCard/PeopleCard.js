import React from 'react';
import { getFollowing } from 'lib/user';
import PropTypes from 'prop-types';
import Spinner from "../../../../components/Spinner";
import {Card} from "vendor/bulma";
import { orderBy } from 'lodash-es';
import {getWorldStats} from "../../../../lib/stats";
import UserRow from "../UserRow";
import Emoji from "../../../../components/Emoji";
import RecentDiscussionList from '../../../discussions/components/RecentDiscussionList';
import TrendingDiscussionList from '../../../discussions/components/TrendingDiscussionList';
import { getRecentlyLaunched } from '../../../../lib/products';
import ProductList from '../../../products/components/ProductList/ProductList';
import { UserMedia } from 'features/users';
import {fetchStreamers} from "../../../../lib/integrations/shipstreams";
import Avatar from "../Avatar/Avatar";
import {Media} from "../../../../vendor/bulma";
import FullName from "../FullName";
import {Link} from "react-router-dom";

class PeopleCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            failed: false,
            following: null,
            topUsers: null,
            newUsers: null,
            streamers: null,
            followingCollapsed: true,
            hasNoFollows: false,
            recentlyLaunched: null,
        }
    }

    async componentDidMount() {
        if (this.props.worldStats) {
            await this.setState({
                topUsers: this.props.worldStats.top_users,
                newUsers: this.props.worldStats.new_users,
                loading: false,
                failed: false,
            })
        }

        await this.fetchPeople()

        if (this.props.withStreamers) {
            await this.fetchStreamers();
        }

        if (this.props.recentlyLaunched) {
            await this.fetchRecentlyLaunched()
        }
    }

    fetchPeople = async () => {
        this.setState({ loading: true })
        if (this.props.withFollowing) {
            await this.fetchFollowing();
        }

        if (!this.state.topUsers || !this.state.newUsers) {
            await this.fetchWorldUsers();
        }

        this.setState({ loading: false, })
    }



    fetchStreamers = async () => {
        try {
            const streamers = await fetchStreamers();
            this.setState({ streamers })
        } catch (e) {
            this.setState({ loading: false, failed: true, streamers: null });
        }
    }


    collapseFollowing = () => {
        this.setState({
            followingCollapsed: !this.state.followingCollapsed
        })
    }

    fetchFollowing = async () => {
        try {
            let following = await getFollowing();
            if (following.length > 0) {
                following = following.filter(u => u.week_tda > 0)
                following = orderBy(following, 'streak', 'desc');
            }
            this.setState({
                following: following,
            });
        } catch (e) {
            this.setState({ loading: false, failed: true, following: null });
        }
    }

    fetchWorldUsers = async () => {
        try {
            let worldStats = await getWorldStats()
            let topUsers = worldStats.top_users;
            let newUsers = worldStats.new_users;
            let makerOfTheDay = worldStats.maker_of_the_day;
            this.setState({
                makerOfTheDay,
                topUsers,
                newUsers: this.props.newUsers ? newUsers : null,
            });
        } catch (e) {
            this.setState({ loading: false, failed: true, topUsers: null, newUsers: null });
        }
    }

    fetchRecentlyLaunched = async () => {
        try {
            let recentlyLaunched = await getRecentlyLaunched()
            this.setState({
                recentlyLaunched
            });
        } catch (e) {
            this.setState({ loading: false, failed: true, recentlyLaunched: null });
        }
    }

    renderMakerOfTheDay = () => {
        if (!this.state.makerOfTheDay || !this.props.withMakerOfTheDay) return null;

        return (
            <div>
                <p className={"heading"}><Emoji emoji="🌟" /> Maker of the day</p>
                <div style={{marginBottom: 10, marginTop: 5, padding: 5}}>
                    <UserMedia medium user={this.state.makerOfTheDay} />
                </div>
            </div>
        )
    }

    renderTopUsers() {
        if (!this.state.topUsers || !this.state.topUsers.length) return null;

        return (
            <div>
                <p className={"heading"}><Emoji emoji="🔥" /> Top streaks</p>
                <UserRow users={this.state.topUsers} />
            </div>
        )
    }

    renderNewUsers() {
        if (!this.state.newUsers || !this.state.newUsers.length) return null;

        return (
            <div>
                <p className={"heading"}><Emoji emoji="👋" /> New friends</p>
                <UserRow users={this.state.newUsers} />
            </div>
        )
    }

    renderRecentlyLaunched() {
        if (!this.state.recentlyLaunched || !this.state.recentlyLaunched.length) return null;

        return (
            <div>
                <p className={"heading"}><Emoji emoji="🚀" /> Recently launched</p>
                <div style={{marginTop: 15}}>
                    <ProductList products={this.state.recentlyLaunched.filter(product => product.launched_at !== null).slice(0, 6)} thumbnail />
                </div>
            </div>
        )
    }

    renderStreamers() {
        if (!this.state.streamers || !this.state.streamers.length || !this.props.withStreamers) return null;

        return (
            <div>
                <p className={"heading"}><Emoji emoji="🔴" /> {this.state.streamers.length} live now</p>
                <div style={{marginTop: 0, marginBottom: 20}}>
                    {this.state.streamers.map(user => (
                        <Media
                            style={{width: "100%", borderTop: 'none', marginTop: 0, paddingTop: 0}}>
                            <Media.Left>
                                <Link to={`/@${user.username}`}>
                                    <Avatar is={48} user={user} />
                                </Link>
                            </Media.Left>
                            <Media.Content style={{width: "100%"}}>
                                <div className={"username-wrapper"} style={{marginBottom: 3}}>
                                    <strong style={{fontFamily: "Poppins, sans-serif"}} is={"6"}>
                                        <FullName user={user} />
                                    </strong>
                                </div>
                                <Link className={"button is-small is-rounded"} to={`/live`}>
                                    <span>Watch now</span>
                                </Link>
                            </Media.Content>
                        </Media>
                    ))}
                </div>
            </div>
        )
    }

    renderDiscussions = () => {
        if (this.props.withDiscussions) {
            return (
                <div>
                    <p className="heading"><Emoji emoji="💬 "/> {this.props.trendingDiscussionsOnly ? 'Trending' : 'Recent'} discussions</p>
                    {this.props.trendingDiscussionsOnly ? <TrendingDiscussionList /> : <RecentDiscussionList />}
                </div>
            )
        } else {
            return null;
        }
    }

    render() {
        if (this.state.loading) {
            return <Card><Card.Content><Spinner small={true} /></Card.Content></Card>
        } else if (this.state.failed) {
            return <Card><Card.Content>Failed to load people. <button onClick={this.fetchPeople}>Retry.</button></Card.Content></Card>
        }

        return (
           <Card>
                <Card.Content>
                    {this.renderMakerOfTheDay()}
                    {this.renderTopUsers()}
                    {this.renderStreamers()}
                    {this.renderNewUsers()}
                    {this.renderRecentlyLaunched()}
                    {this.renderDiscussions()}
                </Card.Content>
           </Card>
        )
    }
}

PeopleCard.propTypes = {
    worldStats: PropTypes.object.isRequired,
}

PeopleCard.defaultProps = {
    withMakerOfTheDay: true,
    withStreamers: true,
}

export default PeopleCard;