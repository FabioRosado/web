import React from 'react';
import PropTypes from 'prop-types';
import FullName from "../FullName";
import Tda from '../../../../components/Tda';
import Streak from '../../../../components/Streak';
import {Card, Level, Media, SubTitle, Tag, Title, Image, Content} from "vendor/bulma";
import './UserMedia.css';
import Emoji from "components/Emoji";
import Avatar from "../Avatar";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import ProfileModalAction from "../../containers/ProfileModalAction";
import GoldIcon from "../../../../GoldIcon";
import MakerScore from "../../../../components/MakerScore";
import {Link} from "react-router-dom";

const Badge = (props) => (
    <span className={`Badge tag is-rounded ${props.className ? props.className : null}`} style={{backgroundColor: props.backgroundColor ? props.backgroundColor : null}}>
        {props.children}
    </span>
)

const GoldBadge = (props) => (
    <Badge className={"is-dark"} backgroundColor={"rgb(229,193,0)"}>
        <FontAwesomeIcon icon={'check-circle'} color="white" />&nbsp;<strong>Gold</strong>
    </Badge>
)

const DonorBadge = (props) => (
    <Badge className={"is-dark"}>
        <Emoji emoji={"✌️"} /> Donor
    </Badge>
)

const TesterBadge = (props) => (
    <Badge className={"is-white"} backgroundColor={"#fdcb6e"}>
        <Emoji emoji={"🚧"} /> Tester
    </Badge>
)

const StaffBadge = (props) => (
    <Badge className={"is-dark"} backgroundColor={"#11998e"}>
        <Emoji emoji={"🛠"} /> Staff
    </Badge>
)

const AwesomeBadge = (props) => (
    <Badge className={"is-dark"} backgroundColor={"#2d98da"}>
        <Emoji emoji={"💫"} /> Awesome
    </Badge>
)

const InlineLevel = (props) => (
    <Level style={{display: "inline-block", marginBottom: 0}} className={"is-inline-flex"}>
        {props.children}
    </Level>
)


const UserBadges = ({ user }) => {
    return (
        <InlineLevel>
            {user.gold && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <GoldBadge />
            </Level.Item>}
            {(user.username.toLowerCase() === 'booligoosh' || user.username.toLowerCase() === 'tomaswoksepp') && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <AwesomeBadge />
            </Level.Item>}
            {user.is_staff && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <StaffBadge />
            </Level.Item>}
            {user.donor && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <DonorBadge />
            </Level.Item>}
            {user.tester && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <TesterBadge />
            </Level.Item>}
        </InlineLevel>
    )
}


const StatBar = ({ user, showUsername=false }) => (
    <div>
        {showUsername && <small>@{user.username}</small>}&nbsp;
        <Tag className={"is-rounded"}><Streak style={{fontWeight: 'bold'}} days={user.streak} /></Tag>&nbsp;
        <Tag className={"is-rounded"}><Tda tda={user.week_tda} /></Tag>
        <UserBadges user={user} />
    </div>
)

const StatText = ({user}) => (
    <small>
        <InlineLevel>
            <Level.Item style={{paddingRight: 5, paddingLeft: 5}}>
                <Tag className={"is-rounded"}><Streak days={user.streak} /></Tag>
            </Level.Item>
            <Level.Item style={{paddingRight: 5, paddingLeft: 5}}>
                <Tag className={"is-rounded"}><Tda tda={user.week_tda} /></Tag>
            </Level.Item>
        </InlineLevel>
    </small>
)

class UserMedia extends React.Component {

    render() {
        let { user } = this.props;
        if (this.props.large) {
            return (
                <div style={this.props.style}>
                    <ProfileModalAction user={this.props.user}>
                        <Card style={{"cursor": "pointer"}}>
                            <Card.Content>
                                <Media>
                                    <Media.Left>
                                        <Image className="epic-border" is='48x48' src={this.props.user.avatar} alt="User avatar" />
                                    </Media.Left>
                                    <Media.Content>
                                        <Title is='4'><FullName user={this.props.user} /></Title>
                                        <SubTitle is='6'>@{this.props.user.username}</SubTitle>
                                    </Media.Content>
                                </Media>
                                <Content>
                                    {this.props.user.description ? this.props.user.description : 'This user has no bio.'}
                                </Content>
                            </Card.Content>
                            <footer className="card-footer">
                                <p className="card-footer-item">
                                    <StatBar user={this.props.user} />
                                </p>
                            </footer>
                        </Card>
                    </ProfileModalAction>
                </div>
            )
        }

        if (this.props.xs) {
            return (
                <div>
                    <ProfileModalAction user={this.props.user}>
                        <Media
                            className={"UserMedia xs" + (!this.props.user.streak || !this.props.user.week_tda ? 'lazy' : '')}>
                            <Media.Left>
                                <Avatar is={48} user={this.props.user} />
                            </Media.Left>
                            <Media.Content>
                                <div>
                                    <strong>
                                        <FullName user={this.props.user} />
                                    </strong>
                                    <br />
                                    <StatText user={this.props.user} />
                                </div>
                            </Media.Content>
                        </Media>
                    </ProfileModalAction>
                </div>
            )
        }

        if (this.props.medium) {
            return (
                <Link style={{color: 'inherit', textDecoration: 'none'}} to={`@${user.username}`}>
                    <Media
                        style={{width: "100%"}}
                        className={"UserMedia " + (!this.props.user.streak || !this.props.user.week_tda ? 'lazy' : '')}>
                        <Media.Left>
                            <Avatar is={48} user={this.props.user} />
                        </Media.Left>
                        <Media.Content style={{width: "100%"}}>
                            <div className={"username-wrapper"}>
                                <div style={{marginBottom: 5}}>
                                    <Title is={"6"}>
                                        <FullName user={user} /> {user.gold && <GoldIcon />}
                                    </Title>
                                    <SubTitle is={"6"}>
                                        <small>{user.description}</small>
                                    </SubTitle>
                                </div>
                                <div className={"stats-case"}>
                                    {user.streak > 0 && <Streak days={user.streak} />} {user.streak === 100 && <Emoji emoji={"🎉"} />} <MakerScore score={user.maker_score}/> <Tda tda={user.week_tda} />
                                </div>
                            </div>
                        </Media.Content>
                    </Media>
                </Link>
            )
        }

        return (
            <Link style={{color: 'inherit', textDecoration: 'none'}} to={`/@${user.username}`}>
                <Media
                    style={{width: "100%"}}
                    className={"UserMedia " + (!this.props.user.streak || !this.props.user.week_tda ? 'lazy' : '')}>
                    <Media.Left>
                        <Avatar is={32} user={this.props.user} />
                    </Media.Left>
                    <Media.Content style={{width: "100%"}}>
                        <div className={"username-wrapper"}>
                            <div>
                                <strong>
                                    <FullName user={user} />
                                </strong> {user.gold &&
                            <GoldIcon />
                            }
                            </div>
                            <div className={"stats-case"}>
                                {user.is_live && <span><Emoji emoji={"🔴"} /> <span className={"has-text-danger"}>LIVE</span></span>} {user.streak > 0 && <Streak days={user.streak} />} {user.streak === 100 && <Emoji emoji={"🎉"} />} <MakerScore score={user.maker_score}/> <Tda tda={user.week_tda} />
                            </div>
                        </div>
                    </Media.Content>
                </Media>
            </Link>
        )
    }
}

UserMedia.propTypes = {
    user: PropTypes.shape({
        first_name: PropTypes.string,
        username: PropTypes.string.isRequired,
        last_name: PropTypes.string,
        streak: PropTypes.number,
    })
}

export default UserMedia;