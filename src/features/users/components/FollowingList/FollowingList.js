import React from 'react';
import { getFollowing } from 'lib/user';
import Spinner from "../../../../components/Spinner";
import {Card, SubTitle, Button, Icon} from "vendor/bulma";
import { orderBy } from 'lodash-es';
import UserMedia from "../UserMedia";
import {getWorldStats} from "../../../../lib/stats";
import {Link} from "react-router-dom";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

const FollowingCard = ({ hasFollows, children }) => (
    <Card>
        <Card.Header>
            <Card.Header.Title>
                <strong>{hasFollows ? 'Following' : 'Makers to follow'}</strong>
            </Card.Header.Title>
        </Card.Header>
        <Card.Content style={{padding: 20, paddingTop: 10, paddingBottom: 10, maxHeight: 400, overflowY: "auto"}}>
            <div style={{marginTop: 10, marginBottom: 10}}>
                {children}
            </div>
        </Card.Content>
        {!hasFollows &&
            <footer className="card-footer">
                <div className={'card-footer-item'}>
                    <Link to={'/explore/makers'}>
                        <Button text>
                            <Icon>
                                <FontAwesomeIcon icon={'users'} />
                            </Icon>
                            <span>Discover more makers &raquo;</span>
                        </Button>
                    </Link>
                </div>
            </footer>
        }
    </Card>
)

class FollowingList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            failed: false,
            people: [],
            hasNoFollows: false,
        }
    }

    async componentDidMount() {
        this.fetchFollowing()
    }

    fetchFollowing = async () => {
        try {
            let people = await getFollowing();
            people = orderBy(people, 'streak', 'desc');
            if (people.length === 0) {
                let worldStats = await getWorldStats()
                people = worldStats.top_users;
                this.setState({ loading: false, failed: false, people: people, hasNoFollows: true });
            } else {
                this.setState({ loading: false, failed: false, people: people });
            }
        } catch (e) {
            this.setState({ loading: false, failed: true, people: [] });
        }
    }

    render() {
        if (this.state.loading) {
            return <FollowingCard hasFollows={!this.state.hasNoFollows}><Spinner text={"Loading makers..."} small={true} /></FollowingCard>
        } else if (this.state.failed) {
            return <FollowingCard hasFollows={!this.state.hasNoFollows}>Failed to load your following. <button onClick={this.fetchFollowing}>Retry.</button></FollowingCard>
        }

        return (
            <FollowingCard hasFollows={!this.state.hasNoFollows}>
                {this.state.people.length === 0 &&
                    <center><SubTitle is="5">You aren't following anyone yet.</SubTitle></center>
                }

                {this.state.people.map(
                    (user) => <div style={{marginTop: 10, marginBottom: 10}}><UserMedia user={user} /></div>
                )}
            </FollowingCard>
        )
    }
}

FollowingList.propTypes = {}

export default FollowingList;