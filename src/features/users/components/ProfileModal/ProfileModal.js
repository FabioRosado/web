import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Title, SubTitle, Button } from 'vendor/bulma';
import ActivitySparklines from 'features/stats/components/ActivitySparklines';
import FullName from '../FullName';
import GatedFollowButton from '../GatedFollowButton';
import {ProductList, ProductsContainer} from "features/products";

// make low performance css and include conditionally

const ProfileModal = (props) => (
    <Modal
        isOpen={props.isOpen}
        onRequestClose={props.onRequestClose}
        className="ProfileModal"
        overlayClassName="ProfileModalOverlay">
        <div className="ProfileModal-Container">
            <div className="ProfileModal-Column ProfileModal-Header">
                <div>
                    <Link to={`/@${props.user.username}`}>
                        <img className="ProfileModal-Avatar" src={props.user.avatar} alt={props.user.username} />
                    </Link>
                    <Title is="3"><FullName user={props.user} /></Title>
                    <SubTitle is="6">{props.user.description ? props.user.description : "I don't have a bio yet."}</SubTitle>
                    <GatedFollowButton userId={props.user.id} />
                </div>
            </div>
            <div className="ProfileModal-Column ProfileModal-Content">
                <SubTitle is="5"><FullName user={props.user} />'s Activity</SubTitle>
                <ActivitySparklines trend={props.user.activity_trend} />
                <hr />
                <SubTitle is="5">Products</SubTitle>
                <p style={{ maxHeight: 100, overflowY: "auto"}}>
                    <ProductsContainer user={props.user.id} component={(props) => <ProductList thumbnail {...props} />} />
                </p>
                <hr />
                <center>
                    <Link to={`/@${props.user.username}`}>
                        <Button text>View full profile &raquo;</Button>
                    </Link>
                </center>
            </div>
        </div>
    </Modal>
)

ProfileModal.propTypes = {
    user: PropTypes.object.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
    me: state.user.me,
    isLoggedIn: state.auth.loggedIn,
})

export default connect(
    mapStateToProps
)(ProfileModal);