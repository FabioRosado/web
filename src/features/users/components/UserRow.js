import React from 'react';
import Avatar from "./Avatar/index";
import {Level} from "vendor/bulma";
import {Link} from "react-router-dom";

const InlineLevel = (props) => (
    <Level style={{display: "inline-block", marginBottom: 0, flexWrap: 'wrap'}} className={"is-inline-flex"}>
        {props.children}
    </Level>
)

export const UserRow = ({ users, withBadge=true, withMargin=true, }) => {
    return users ? (
        <div style={withMargin ? {marginTop: 5, marginBottom: 20} : {}}>
            <InlineLevel>
                {users.map(u =>
                    <Level.Item key={u.id} style={{paddingRight: 5, paddingLeft: 5, paddingTop: 10}}>
                        <Link to={`/@${u.username}`}>
                            <Avatar user={u} is={48} withBadge={withBadge} />
                        </Link>
                    </Level.Item>
                )}
            </InlineLevel>
        </div>
    ) : null;
}

export default UserRow;