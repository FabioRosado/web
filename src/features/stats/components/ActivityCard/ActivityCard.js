import React from 'react';
import PropTypes from 'prop-types';
import {Card} from "vendor/bulma";
import ActivitySparklines from "../ActivitySparklines";
import Streak from 'components/Streak';
import Tda from 'components/Tda';

const ActivityCardPanel = (props) => (
    <Card>
        <Card.Header>
            <Card.Header.Title>
                Your activity
            </Card.Header.Title>
        </Card.Header>
        {props.children}
    </Card>
)

const ActivityCard = (props) => {
    let trend = [0]
    if (props.trend) {
        trend = props.trend;
    } else {
        // Falls back to user trend, from passed user object. This may be why live update doesn't work.
        trend = props.user.activity_trend ? props.user.activity_trend : [0];
    }

    return (
        <Card>
            <Card.Content>
                <center>
                    <p className={"heading has-text-weight-bold"}>{props.heading ? props.heading : 'Your activity'}</p>
                </center>
                <ActivitySparklines trend={trend} />
            </Card.Content>
            {props.user && props.showOtherStats &&
                <footer className="card-footer">
                    <p className="card-footer-item">
                      <span>
                          <Tda tda={props.user.week_tda} /> tasks/day
                      </span>
                    </p>
                    <p className="card-footer-item">
                      <span>
                          <Streak days={props.user.streak} />
                      </span>
                    </p>
                </footer>
            }
        </Card>
    )
}

ActivityCard.propTypes = {
    user: PropTypes.shape({
        activity_trend: PropTypes.array.isRequired,
        streak: PropTypes.number.isRequired,
        week_tda: PropTypes.number.isRequired
    }),
    trend: PropTypes.array,
}

ActivityCard.defaultProps = {
    showOtherStats: false
}

export {
    ActivityCard,
    ActivityCardPanel
}