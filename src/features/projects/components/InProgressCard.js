import React from 'react';
import PropTypes from 'prop-types';
import {groupTasksByDone} from "lib/utils/tasks";
import {Card, Heading, Level, Title} from "vendor/bulma";
import {Entry} from "features/stream";
import {Tooltip} from "react-tippy";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {StatsLevel} from "features/stats";
import { connect } from 'react-redux';
import mapDispatchToProps from '../containers/mapDispatchToProps';
import {FullName} from "features/users";
import Greeting from "components/Greeting";


const HelpTooltip = (props) => (
    <Tooltip
        html={<span><FontAwesomeIcon icon={'check'} /> <b>Click</b> to mark as done</span>}
        delay={200}
        position={'right'}
        size={'small'}
        hideOnClick
        sticky>
        {props.children}
    </Tooltip>
)


const InProgressCard = (props) => {
    if (!props.showGreeting && (props.tasks.length === 0 || !props.tasks)) return null;

    return (
        <Card className={"InProgressCard " + (props.showGreeting && props.tasks.length === 0 && 'empty')}>
            <Card.Content>
                <Level>
                    <Level.Left>
                        <Level.Item>
                            <div>
                                {props.tasks.length > 0 && <Heading>In progress</Heading>}
                                {props.showGreeting && props.tasks.length === 0 &&
                                <div>
                                    <Title is="5" className={"has-text-white"} style={{marginBottom: 4}}>
                                        <Greeting />, <FullName user={props.user} />
                                    </Title>
                                    <small>
                                        Mark a task below as "In Progress" to see it here.
                                    </small>
                                </div>
                                }
                                {props.tasks.map(
                                    task =>
                                        <div onClick={() => props.markDone(task.id)}>
                                            <Entry
                                                contentWrapper={HelpTooltip}
                                                task={task}
                                                withCounts={false}
                                                key={task.id}
                                                withTooltip={false}
                                                withDetailModal={false}
                                                withPraise={false} />
                                        </div>
                                )}
                            </div>
                        </Level.Item>
                    </Level.Left>
                    {props.large &&
                        <Level.Right>
                            <Level.Item>
                                <StatsLevel className={"has-text-white"} large={false} />
                            </Level.Item>
                        </Level.Right>
                    }
                </Level>
            </Card.Content>
        </Card>
    )
}

InProgressCard.propTypes = {
    showGreeting: PropTypes.bool,
}

InProgressCard.defaultProps = {
    showGreeting: true
}

export default connect(
    (state) => ({
        user: state.user.me,
        tasks: groupTasksByDone(state.tasks.tasks).in_progress
    }),
    mapDispatchToProps
)(InProgressCard)
