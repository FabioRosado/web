import React from "react";
import {connect} from "react-redux";
import { actions as tasksActions } from 'ducks/tasks';
import {applySearchTerms} from "lib/utils/tasks";
import {Card, SubTitle, Title} from "vendor/bulma";
import differenceInHours from 'date-fns/differenceInHours';
import { Task } from "features/stream";
import styled from 'styled-components';
import {Button, Icon, Level} from "../../../vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Emoji from "../../../components/Emoji";
import OutboundLink from "../../../components/OutboundLink";
import {Tooltip} from "react-tippy";
import {actions as editorActions} from "../../../ducks/editor";

const TodayPage = styled.div`
  min-height: ${props => props.focusMode ? '100vh' : 'calc(100vh - 120px)'};
  margin-top: 0px;
  position: relative;
  overflow: hidden;
  
  small {
    color: white;
  }
  
  h1 {
    color: white;
  }
  
  .button.is-text {
    color: white;
    text-decoration: none;
    background-color: transparent !important;
  }
  .button.is-text:hover, .button.is-text.is-active {
    background-color: transparent;
  }
  
  & > .subtitle {
    opacity: 0.5;
  }
  
  .actions {
    margin-top: 10px;
    opacity: 0.5;
  }
  
    &::before {
        display: block;
        position: absolute;
        content: '';
        background: ${props => props.theme.primaryDarker};
        background-image: url(${props => props.user.header});
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        width: 120%; 
        height 120%; 
        margin: -10%;
    }
    
    .column > div {
      width: 100%;
    }
  
  
  .column {
    padding-top: 30px;
    z-index: 100;
    ${props => props.focusMode ? "display: flex; align-items: center; justify-content: center;" : ""}
  }
  
  .Entry {
    cursor: pointer;
  }
  
  .overlay { 
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.3);
  }
  
  .message-container { 
    padding: 20px;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }
  
  .message-container > .subtitle {
    margin-bottom: 0px;
  }
  
  .message-container > div {
    margin-top: 15px;
  }
`;

class TodayView extends React.Component {
    state = {
        focusMode: false,
    }

    toggleFocusMode = () => {
        if (this.state.focusMode) {
            this.setState({
                focusMode: false
            })
            document.getElementById('root').classList.remove('nav-hidden')
        } else {
            document.getElementById('root').classList.add('nav-hidden');
            this.setState({
                focusMode: true
            })
        }
    }

    toggleFullScreen = () => {
        if (!document.fullscreenElement) {
            document.documentElement.requestFullscreen();
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
        }
    }

    renderTweetButton = () => {
        const text = `Today I completed ${this.props.doneToday} tasks on @getmakerlog! 💪 \n #TogetherWeMake`;
        const url = `${process.env.REACT_APP_BASE_URL}/@${this.props.me.username}`;

        return (
            <OutboundLink href={`https://twitter.com/share?text=${encodeURIComponent(text)}&url=${url}`}
                          className="button is-info is-small is-rounded" style={{backgroundColor: "#1b95e0"}} target="_blank">
                <Icon><FontAwesomeIcon icon={['fab', 'twitter']} /></Icon> Tweet your victory
            </OutboundLink>
        )
    }

    render() {
        if (!this.props.tasks) return null;
        const tasks = this.props.tasks.filter(
            task => (
                differenceInHours(
                    new Date(),
                    new Date(task.created_at)
                ) <= 24 && task.done === false
            )
        )

        return (
            <TodayPage user={this.props.me} focusMode={this.state.focusMode} className={"columns"}>
                <div className={"overlay"}></div>
                <div className="column is-one-third is-offset-one-third">
                    <div>
                        <Level>
                            <Level.Left>
                                <div>
                                    <Title is={"5"}>
                                        Remaining today
                                    </Title>
                                </div>
                            </Level.Left>
                            <Level.Right>
                                <FontAwesomeIcon icon={"plus"} color={"white"} onClick={this.props.toggleEditor} />
                            </Level.Right>
                        </Level>
                        <Card>
                            <Card.Content>
                                {this.props.doneToday === 0 && tasks.length === 0 &&
                                    <div className={"message-container"}>
                                        <SubTitle>You haven't done anything today.</SubTitle>
                                        <div>
                                            <Button className={"is-rounded"} primary small  onClick={this.props.toggleEditor}>
                                                <Icon><FontAwesomeIcon icon={"plus"} color={"white"} /></Icon> Add tasks
                                            </Button>
                                        </div>
                                    </div>
                                }
                                {tasks.length === 0 && this.props.doneToday > 0 &&
                                    <div className={"message-container"}>
                                        <SubTitle><Emoji emoji={"🎉"} /> All done for today! </SubTitle>
                                        <div>{this.renderTweetButton()}</div>
                                    </div>
                                }
                                {tasks.map(
                                    task => (
                                        <div onClick={e => this.props.markDone(task.id)}>
                                            <Tooltip
                                                html={<span><FontAwesomeIcon icon={'check'} /> <b>Click</b> to mark as done</span>}
                                                delay={1000}
                                                position={'right'}
                                                size={'small'}
                                                hideOnClick
                                                followCursor>
                                                <Task withTooltip={false} withPraise={false} withDetailModal={false} withCounts={false} task={task} />
                                            </Tooltip>
                                        </div>
                                    )
                                )}
                            </Card.Content>
                        </Card>
                        <Level className={"actions"}>
                            <Level.Left>
                                <Level.Item>
                                    <Button text small onClick={this.toggleFocusMode}>
                                        <Icon><FontAwesomeIcon icon={"eye"} /></Icon> <span>{this.state.focusMode ? 'Exit focus mode' : 'Focus mode'}</span>
                                    </Button>
                                </Level.Item>
                                <Level.Item>
                                    <Button text small onClick={this.toggleFullScreen}>
                                        <Icon><FontAwesomeIcon icon={"arrows-alt"} /></Icon> <span>Toggle full screen</span>
                                    </Button>
                                </Level.Item>
                            </Level.Left>
                        </Level>
                    </div>
                </div>
            </TodayPage>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tasks: applySearchTerms(state.tasks.tasks, state.tasks.searchTerms),
        projects: state.projects.projects,
        isSyncing: state.tasks.isSyncing || state.projects.isSyncing,
        isSilentlySyncing: state.tasks.ready && state.tasks.isSyncing,
        failed: state.tasks.failed,
        errorMessages: state.tasks.errorMessages,
        ready: state.tasks.ready && state.projects.ready,
        searchTerms: state.tasks.searchTerms,
        taskView: state.tasks.taskView,
        me: state.user.me,
        remainingTasks: state.stats.user.remaining_tasks,
        doneToday: state.stats.user.done_today,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadTasks: () => dispatch(tasksActions.loadTasks()),
        markDone: id => dispatch(tasksActions.markDone(id)),
        toggleEditor: () => dispatch(editorActions.toggleEditor())
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TodayView);
