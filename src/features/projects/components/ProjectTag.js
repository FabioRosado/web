import React from 'react';
import { Tag } from 'vendor/bulma';
import capitalize from 'capitalize';

const ProjectTag = ({ name }) => (
    <Tag>
        <strong>{capitalize(name).replace(/_/g, " ")}</strong> &nbsp; #{name.toLowerCase()}
    </Tag>
);

export default ProjectTag;