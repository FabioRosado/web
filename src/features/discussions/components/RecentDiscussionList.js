import React from "react";
import {getRecentDiscussions} from "../../../lib/discussions";
import {Link} from "react-router-dom";
import {Level, SubTitle, Button} from "vendor/bulma";
import Spinner from "../../../components/Spinner";
import ReplyFaces from './ReplyFaces';

class RecentDiscussionList extends React.Component {
    state = {
        loading: true,
        data: null,
        failed: false,
    }

    async fetchThreads() {
        this.setState({ loading: true, failed: false, })
        let data = null;
        try {
            data = await getRecentDiscussions();
            this.setState({ data: data, loading: false, failed: false, })
        } catch (e) {
            this.setState({ data: null, loading: false, failed: true, })
        }
    }

    componentDidMount() {
        this.fetchThreads();
    }

    onEditProduct = () => {
        this.fetchThreads();
    }

    render() {

        if (this.state.loading) {
            return <Spinner small={true} />;
        }

        if (this.state.failed && this.state.loading === false) {
            return <center><SubTitle>Failed to load threads. <Button onClick={() => this.fetchThreads()}>Try again &raquo;</Button></SubTitle></center>
        } else if (!this.state.loading && !this.state.failed) {
            // You can optionally pass these to child products (onEdit and onDelete) if you want functionality like this in your view. Works with cards.
            // ProductList picks this up automatically.
            return (
                <div className={"RecentQuestionsList"}>
                    {this.state.data.map((thread) => (
                        <Link to={`/discussions/${thread.slug}`}>
                            <div>
                                <h2 className={"question-title"}>
                                    {thread.title}
                                </h2>
                                <Level>
                                    <Level.Left>
                                        <Level.Item className={"has-text-grey-light"}>
                                            {thread.type === 'QUESTION' ? `${thread.reply_count} answers` : `${thread.reply_count} replies`}
                                        </Level.Item>
                                        <Level.Item>
                                            <ReplyFaces maxFaces={6} withOwner threadSlug={thread.slug} />
                                        </Level.Item>
                                    </Level.Left>
                                </Level>
                            </div>
                        </Link>
                    ))}
                </div>
            )
        }
    }
}

export default RecentDiscussionList;