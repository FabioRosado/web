import React from 'react';
import Product from './Product';
import {Heading} from "vendor/bulma";
import {Tooltip} from "react-tippy";

const ProductTooltipHtml = ({ product }) => (
    <div style={{ width: 300, padding: 10, fontSize: 16, textAlign: 'left' }}>
        <Heading>Product details</Heading>
        <Product media product={product} />
    </div>
)

const ProductTooltip = ({ product, children }) => (
    <Tooltip
        interactive
        useContext
        html={
            <ProductTooltipHtml product={product} />
        }
        delay={300}
        position={'top'}
        size={'small'}>
        {children}
    </Tooltip>
)

export default ProductTooltip;