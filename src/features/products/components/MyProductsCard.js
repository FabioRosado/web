import React from 'react';
import Spinner from "components/Spinner";
import {Card, SubTitle, Heading, Button} from "vendor/bulma";
import {getMyProducts} from "lib/products";
import { Link } from 'react-router-dom';
import {ProductList} from "features/products";
import Emoji from "components/Emoji";

const ProductsCardContainer = (props) => (
    <Card>
        <Card.Content>
            <Heading>
                <Emoji emoji={"💻"} /> Your Products
            </Heading>
            <div>
                {props.children}
            </div>
        </Card.Content>
    </Card>
)

class MyProductsCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            failed: false,
            products: []
        }
    }

    getProducts = async () => {
        try {
            let products = await getMyProducts();
            this.setState({ loading: false, failed: false, products: products });
        } catch (e) {
            this.setState({ loading: false, failed: true, products: [] });
        }
    }

    async componentDidMount() {
        this.getProducts();
    }

    render() {
        if (this.state.loading) {
            return <ProductsCardContainer><Spinner text={"Loading products..."} small={true} /></ProductsCardContainer>
        } else if (this.state.failed) {
            return <ProductsCardContainer>Failed to load your products. <Button onClick={this.getProducts}>Retry.</Button></ProductsCardContainer>
        }

        return (
            <ProductsCardContainer>
                {this.state.products.length === 0 &&
                    <div>
                        <SubTitle className="has-text-grey-light" is="6">You haven't added any products yet.</SubTitle>
                        <Link className="button is-small is-rounded" to='/products'>Add a product</Link>
                    </div>
                }
                {this.state.products.length > 0 &&
                    <div style={{marginTop: 10}}>
                        <ProductList products={this.state.products} thumbnail />
                    </div>
                }
            </ProductsCardContainer>
        )
    }
}

MyProductsCard.propTypes = {}

export default MyProductsCard;