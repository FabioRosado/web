import React from "react";
import {deleteProduct, editProduct, getProductBySlug, leaveProduct} from "lib/products";
import {Button, Control, Field, Image, Message, SubTitle, Title} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {File} from 'vendor/bulma';
import {Level} from "vendor/bulma";
import {StreamCard} from "features/stream/components/Stream/components/StreamCard/styled";
import {Icon, Input, Tag, Textarea} from "vendor/bulma";

class ProductIconPicker extends React.Component {
    onIconUpload = (event) => {
        const file = event.target.files[0];
        this.props.onIconUpload(file, URL.createObjectURL(file))
    }

    render() {
        return (
            <File name>
                <File.Label>
                    <File.Input accept="image/*" onChange={this.onIconUpload} />
                    <File.Cta>
                        <File.Icon>
                            <FontAwesomeIcon icon={'upload'} />
                        </File.Icon>
                        <File.Label as='span'>
                            Upload…
                        </File.Label>
                    </File.Cta>
                </File.Label>
            </File>
        )
    }
}

export default ProductIconPicker;