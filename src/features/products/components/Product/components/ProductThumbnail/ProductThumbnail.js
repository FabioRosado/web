import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import ProductTooltip from "../../../ProductTooltip";

class ProductThumbnail extends React.Component {
    render() {
        return (
           <Link to={`/products/${this.props.product.slug}`} style={{ display: 'inline-block', marginRight: 5 }}>
               <ProductTooltip product={this.props.product}>
                   <figure className="ProductThumbnail image is-48x48 img-rounded">
                       <img src={this.props.product.icon ? this.props.product.icon : "https://via.placeholder.com/200?text=No+icon"} alt="User" />
                   </figure>
               </ProductTooltip>
           </Link>
        )
    }
}

ProductThumbnail.propTypes = {
    product: PropTypes.object.isRequired,
}

export default ProductThumbnail;