import React from 'react';
import PropTypes from 'prop-types';
import {Heading, Image, Level, Media, SubTitle, Tag, Title} from "vendor/bulma";
import Emoji from "../../../../../../components/Emoji";
import SocialMediaLevel from "../../../../../../components/SocialMediaLevel";
import ProductPeople from "../../../ProductPeople";
import ProductStatsContainer from "../../../../containers/ProductStatsContainer";
import Tda from "components/Tda";

class ProductHero extends React.Component {
    renderHero = (product) => (
        <section className="hero is-primary">
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                        <div className="column ProductHero-Media">
                            <Media>
                                <Media.Left className="ProductHero-Icon">
                                    <Image className={"img-rounded"} is={"128x128"} src={product.icon ? product.icon : "https://via.placeholder.com/500?text=No+icon"} />
                                </Media.Left>
                                <Media.Content>
                                    <Title is="2">
                                        {product.name} {product.launched && <Tag><Emoji emoji={"🚀"} /> Launched</Tag>}
                                    </Title>
                                    <SubTitle>
                                        {product.description ? product.description : "The maker didn't describe this product... poor product."}
                                    </SubTitle>
                                    <SocialMediaLevel
                                        website={product.website}
                                        twitterUser={product.twitter}
                                        productHuntUrl={product.product_hunt}
                                    />
                                </Media.Content>
                            </Media>
                        </div>
                        <div className="column ProductHero-Stats">
                            <Level mobile>
                                <Level.Item hasTextCentered>
                                    <span>
                                        <Heading>Makers</Heading>
                                        <ProductPeople slug={product.slug} size={48} />
                                    </span>
                                </Level.Item>
                                <ProductStatsContainer
                                    slug={product.slug}
                                    component={({ stats }) => (
                                        <>
                                            <Level.Item hasTextCentered>
                                                <span>
                                                    <Heading>Done today</Heading>
                                                    <Title is={'2'}>
                                                        {stats.done_today}
                                                    </Title>
                                                </span>
                                            </Level.Item>
                                            <Level.Item hasTextCentered>
                                                <span>
                                                    <Heading>Tasks/day</Heading>
                                                    <Title is={'2'}>
                                                        <Tda tda={stats.week_tda} />
                                                    </Title>
                                                </span>
                                            </Level.Item>
                                        </>
                                    )}
                                />
                            </Level>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )

    render() {
        return this.renderHero(this.props.product)
    }
}

ProductHero.propTypes = {
    product: PropTypes.object.isRequired,
}

export default ProductHero;