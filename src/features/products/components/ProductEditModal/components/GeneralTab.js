import {deleteProduct, editProduct, getProductBySlug, leaveProduct} from "lib/products";
import React from "react";
import {Button, Control, Field, Image, Message, SubTitle, Title} from "vendor/bulma";
import {File} from 'vendor/bulma';
import {Level} from "vendor/bulma";
import {StreamCard} from "features/stream/components/Stream/components/StreamCard/styled";
import {Icon, Input, Tag, Textarea} from "vendor/bulma";
import ProductIconPicker from "../../ProductIconPicker";
import LaunchedToggle from "../../LaunchedToggle";


export default (props) => (
    <>
        <div className={"columns"}>
            <div className={"column"} style={{maxWidth: "100%"}}>
                <Field>
                    <label className="label">Product name</label>
                    <Control>
                        <Input value={props.name} onChange={props.onNameChange} placeholder="My Amazing Product" />
                    </Control>
                </Field>
            </div>
            <div className={"column is-one-quarter"}>
                <Field>
                    <label className="label">Launched?</label>
                    <Control>
                        <LaunchedToggle
                            launched={props.launched}
                            onLaunchedChange={props.onLaunch}
                        />
                    </Control>
                </Field>
            </div>
        </div>
        <Field>
            <label className="label">Product description</label>
            <Control>
                <Textarea
                    value={props.description}
                    onChange={props.onDescriptionChange}
                    placeholder="This product is an amazing one, it helps create spreadsheets from tables. Actual tables." />
            </Control>
        </Field>
        <Field>
            <label className={"label"}>Icon</label>
            <Control>
                <ProductIconPicker onIconUpload={props.onIconUpload} />
            </Control>
        </Field>
        <hr />
        <Field>
            <label className="label">Website</label>
            <Control>
                <Input
                    value={props.website}
                    onChange={props.onWebsiteChange}
                    placeholder="https://getmakerlog.com" />
            </Control>
        </Field>
        <Field>
            <label className="label">Twitter handle</label>
            <Control>
                <Input
                    value={props.twitter}
                    onChange={props.onTwitterChange}
                    placeholder="getmakerlog" />
            </Control>
        </Field>
        <Field>
            <label className="label">Product Hunt URL</label>
            <Control>
                <Input
                    value={props.productHunt}
                    onChange={props.onProductHuntChange}
                    placeholder="https://producthunt.com/posts/makerlog" />
            </Control>
        </Field>
    </>
)