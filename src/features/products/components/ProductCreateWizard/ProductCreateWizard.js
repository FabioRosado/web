import React from 'react';
import { Title, SubTitle, Button, Icon, Field, Control, File, Message } from 'vendor/bulma';
import { Wizard, Steps, Step } from 'react-albus';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { createProduct } from 'lib/products';
import { isFunction } from 'lodash-es';
import {ProjectPicker} from 'features/projects';
import Emoji from '../../../../components/Emoji';
import {Level} from "../../../../vendor/bulma";
import LaunchedToggle from "../LaunchedToggle";
import { Product } from 'features/products';
import TeamSelector from "../TeamSelector";

const CreateProductStep1 = (props) => (
    <div>
        <Title is="5"><Emoji emoji="✏️ " /> The basics</Title>
        <Field>
            <SubTitle is="6">What's this product called?</SubTitle>
            <Control>
                <input value={props.name} onChange={props.onNameChange} />
            </Control>
        </Field>
        <Field>
            <SubTitle is="6">What does it do?</SubTitle>
            <Control>
                <textarea value={props.description} onChange={props.onDescriptionChange} />
            </Control>
        </Field>
        <Field>
            <SubTitle is="6">Launched yet?</SubTitle>
            <Control>
                <LaunchedToggle launched={props.launched} onLaunchedChange={props.onLaunchedChange} />
            </Control>
        </Field>
        <hr />
        <Level>
            <Level.Left>

            </Level.Left>
            <Level.Right>
                <Button disabled={props.description.length === 0 || props.name.length === 0} onClick={props.next} primary>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-right'} /></Icon> Next
                </Button>
            </Level.Right>
        </Level>
    </div>
)

const CreateProductStep4 = (props) => (
    <div>
        <Title is="5"><Emoji emoji="👩🏼‍💻" /> Extra details</Title>
        <Field>
            <SubTitle is="6">Website?</SubTitle>
            <Control>
                <input placeholder="getmakerlog.com" value={props.url} onChange={props.onUrlChange} />
            </Control>
        </Field>
        <Field>
            <SubTitle is="6">Product Hunt URL?</SubTitle>
            <Control>
                <input placeholder="producthunt.com/posts/makerlog" value={props.productHunt} onChange={props.onproductHuntChange} />
            </Control>
        </Field>
        <Field>
            <SubTitle is="6">Product Twitter handle?</SubTitle>
            <Control>
                <input placeholder="getmakerlog" value={props.twitter} onChange={props.onTwitterChange} />
            </Control>
        </Field>

        {props.errorMessages &&
            <Message danger>
                <Message.Body>
                    {props.errorMessagesRender()}
                </Message.Body>
            </Message>
        }

        <hr />
        <Level>
            <Level.Left>
                <Button onClick={props.previous}>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-left'} /></Icon> Previous
                </Button>
            </Level.Left>
            <Level.Right>
                <Button primary onClick={props.onSubmit} loading={props.loading}>
                    <Icon><FontAwesomeIcon icon={'check'} /></Icon> Finish
                </Button>
            </Level.Right>
        </Level>
    </div>
)

const CreateProductStep3 = (props) => (
    <div>
        <Title is="5"><Emoji emoji="🧐" /> Which hashtags do we track?</Title>
        <ProjectPicker onProjectSelect={props.onProjectSelect} />
        <hr />
        <Level>
            <Level.Left>
                <Button onClick={props.previous}>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-left'} /></Icon> Previous
                </Button>
            </Level.Left>
            <Level.Right>
                <Button disabled={props.selectedProjects && props.selectedProjects.length === 0} onClick={props.next} primary>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-right'} /></Icon> Next
                </Button>
            </Level.Right>
        </Level>
    </div>
)

const CreateProductAddTeam = (props) => (
    <div>
        <Title is="5"><Emoji emoji="🚢" /> Add your team (optional)</Title>
        <div style={{margin: 30, textAlign: 'center'}}>
                <TeamSelector team={props.team} onChange={props.onAddTeamMember} />
        </div>
        <hr />
        <Level>
            <Level.Left>
                <Button onClick={props.previous}>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-left'} /></Icon> Previous
                </Button>
            </Level.Left>
            <Level.Right>
                <Button disabled={props.selectedProjects && props.selectedProjects.length === 0} onClick={props.next} primary>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-right'} /></Icon> Next
                </Button>
            </Level.Right>
        </Level>
    </div>
)

const CreateProductStep2 = (props) => (
    <div>
        <Title is="5"><Emoji emoji="✨" /> Pick an icon (optional)</Title>
            <center>
                <File name boxed>
                    <File.Label style={{width: '100%'}}>
                        <File.Input accept="image/*" onChange={props.onLogoUpload} />
                        <File.Cta>
                            <File.Icon>
                                <FontAwesomeIcon icon={'upload'} />
                            </File.Icon>
                            <File.Label as='span'>
                                Choose a file…
                            </File.Label>
                        </File.Cta>
                    </File.Label>
                </File>
            </center>
            {props.logoPreviewUrl &&
                <div style={{ padding: 20 }}>
                    <Product media product={{
                        "id": 8,
                        "name": props.name,
                        "slug": "makerlog",
                        "user": 1,
                        "product_hunt": "producthunt.com/posts/makerlog-2-0",
                        "twitter": "getmakerlog",
                        "website": "https://getmakerlog.com",
                        "projects": [],
                        "launched": props.launched,
                        "icon": props.logoPreviewUrl,
                        "description": props.description,
                        "created_at": "2018-08-25T07:34:45.992312+08:00",
                        "launched_at": "2018-08-25T07:34:45.987730+08:00"
                    }} />
                </div>
            }
        <hr />
        <Level>
            <Level.Left>
                <Button onClick={props.previous}>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-left'} /></Icon> Previous
                </Button>
            </Level.Left>
            <Level.Right>
                <Button onClick={props.next} primary>
                    <Icon><FontAwesomeIcon icon={'arrow-circle-right'} /></Icon> Next
                </Button>
            </Level.Right>
        </Level>
    </div>
)

class ProductCreateWizard extends React.Component {
    state = {
        isCreating: false,
        finished: false,
        name: '',
        description: '',
        launched: false,
        logo: null,
        logoPreviewUrl: null,
        selectedProjects: [],
        url: '',
        team: [],
        productHunt: '',
        twitter: '',
        errorMessages: null,
    }

    setUrl = (key, url) => {
        let newUrl = url;

        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            newUrl = `https://${url}`
        }
        this.setState({
            [key]: newUrl
        })
    }

    onLogoUpload = (event) => {
        const file = event.target.files[0];
        const reader  = new FileReader();
        reader.readAsDataURL(file);

        reader.onloadend = (e) => {
            this.setState({
                logoPreviewUrl: reader.result
            })
        }

        this.setState({
            logo: file
        })
    }

    onSubmit = async () => {
        try {
            this.setState({ isCreating: true });
            // returns product instance
            await createProduct(
                this.state.name,
                this.state.description,
                this.state.selectedProjects,
                this.state.productHunt,
                this.state.twitter,
                this.state.url,
                this.state.launched,
                this.state.logo,
                this.state.team
            )

            if (isFunction(this.props.onFinish)) {
                this.props.onFinish()
            }
        } catch (e) {
            this.setState({ isCreating: false, errorMessages: e.field_errors || e.message });
        }
    }

    onAddTeamMember = (team) => {
        this.setState({
            team
        })
    }

    renderErrorMessages = () => {
        let messages = [];
        let errors = this.state.errorMessages;
        if (typeof errors === 'object') {
            for (let key in errors) {
                messages.push(
                    <p>
                        <strong>{key.replace(/[_-]/g, " ")}</strong>: {errors[key]}
                    </p>
                )
            }
        } else if (errors.constructor === Array) {
            errors.map((err) => {
                messages.push(
                    <p>{err}</p>
                )

                return true;
            })
        } else {
            messages = this.state.errorMessages;
        }

        return messages
    }

    render() {
        return (
            <div className="CreateProductForm">
                <Wizard>
                    <Steps>
                        <Step
                            id="firstStep"
                            render={({ next }) => (
                                <CreateProductStep1
                                    next={next}
                                    name={this.state.name}
                                    onNameChange={(e) => this.setState({name: e.target.value})}
                                    description={this.state.description}
                                    onDescriptionChange={(e) => this.setState({description: e.target.value})}
                                    launched={this.state.launched}
                                    onLaunchedChange={(e) => this.setState({launched: !this.state.launched})}
                                />
                            )}
                        />
                        <Step
                            id="secondStep"
                            render={({ previous, next }) => (
                                <CreateProductStep2
                                    previous={previous}
                                    next={next}
                                    logoPreviewUrl={this.state.logoPreviewUrl}
                                    logo={this.state.logo}
                                    onLogoUpload={this.onLogoUpload}
                                    launched={this.state.launched}
                                    name={this.state.name}
                                    description={this.state.description}
                                />
                            )}
                        />
                        <Step
                            id="thirdStep"
                            render={({ next, previous }) => (
                                <CreateProductStep3
                                    selectedProjects={this.state.selectedProjects}
                                    previous={previous}
                                    next={next}
                                    onProjectSelect={(projects) => this.setState({selectedProjects: projects})}
                                />
                            )}
                        />
                        <Step
                            id="teamStep"
                            render={({ next, previous }) => (
                                <CreateProductAddTeam
                                    team={this.state.team}
                                    onAddTeamMember={this.onAddTeamMember}
                                    previous={previous}
                                    next={next}
                                />
                            )}
                        />
                        <Step
                            id="fourthStep"
                            render={({ previous }) => (
                                <CreateProductStep4
                                    loading={this.state.isCreating}
                                    previous={previous}
                                    twitter={this.state.twitter}
                                    productHunt={this.state.productHunt}
                                    url={this.state.url}
                                    onTwitterChange={(e) => this.setState({ twitter: e.target.value })}
                                    onproductHuntChange={(e) => this.setUrl('productHunt', e.target.value)}
                                    onUrlChange={(e) => this.setUrl('url', e.target.value)}
                                    onSubmit={this.onSubmit}
                                    errorMessages={this.state.errorMessages}
                                    errorMessagesRender={this.renderErrorMessages}
                                />
                            )}
                        />
                    </Steps>
                </Wizard>
            </div>
        )
    }
}

ProductCreateWizard.propTypes = {}

// TODO: componentize projectpicker

export default ProductCreateWizard;