import React from 'react';
import {Content, Image, Level, Media, Button, Control, Field, Textarea} from "vendor/bulma";
import {Link} from "react-router-dom";
import {FullName} from "features/users";
import TimeAgo from "react-timeago";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Markdown from "components/Markdown";
import {deleteComment, editComment} from "../../../lib/tasks";
import { withCurrentUser } from "features/users";
import {Icon} from "../../../vendor/bulma";

class CommentEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            content: this.props.body ? this.props.body : '',
            failed: false,
        }
    }

    onSubmit = async e => {
        e.preventDefault();
        try {
            this.setState({
                loading: true,
                failed: false,
            })
            await editComment(this.props.comment.task, this.props.comment.id, this.state.content)
            this.setState({
                loading: false,
                failed: false,

            })
            if (this.props.onFinish) this.props.onFinish(this.state.content)
        } catch (e) {
            this.setState({
                loading: false,
                failed: true,
            })
        }
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <Field>
                    <Control>
                        <Textarea value={this.state.content} onChange={e => this.setState({ content: e.target.value })}></Textarea>
                    </Control>
                </Field>
                <Field>
                    <Control>
                        <Button small primary loading={this.state.loading}>
                            {this.state.failed ? 'Failed to edit. Try again later.' : 'Submit'}
                        </Button>
                    </Control>
                </Field>
            </form>
        )
    }
}

class Comment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: false,
            deleted: false,
            body: this.props.comment ? this.props.comment.content : ''
        }
    }

    toggleEditing = e => {
        this.setState({
            editing: !this.state.editing,
        })
    }

    onFinish = body => {
        this.setState({
            body,
            editing: false,
        })
    }

    onDelete = async e => {
        try {
            await deleteComment(this.props.comment.task, this.props.comment.id)
            this.setState({ deleted: true, body: "" })
        } catch (e) {
            this.setState({ deleted: false, body: "" })
        }
    }

    render() {
        const props = this.props;

        return (
            <Media>
                <Media.Left>
                    <Link to={`/@${props.comment.user.username}`}>
                        <Image className="img-circle" is={'32x32'} src={props.comment.user.avatar}/>
                    </Link>
                </Media.Left>
                <Media.Content>
                    <Level className={"CommentInfo has-text-grey is-hidden-touch"}>
                        <Level.Left>
                            <Level.Item>
                                <strong style={{fontSize: 13}}><FullName user={props.comment.user}/></strong>
                            </Level.Item>
                            <Level.Item>
                                <small style={{fontSize: 13}} className={"has-text-grey-light"}>
                                    <TimeAgo date={props.comment.created_at} />
                                </small>
                            </Level.Item>
                        </Level.Left>
                    </Level>
                    <Content>
                        <div style={{marginTop: 5, marginBottom: 5}}>
                            {this.state.editing ?
                                <CommentEditor comment={this.props.comment} body={this.state.body} onFinish={this.onFinish} />
                                : <Markdown body={this.state.body} />
                            }
                            {this.state.deleted && <em>This comment was deleted.</em>}
                        </div>
                        <Level>
                            <Level.Left>
                                {this.props.me.id === this.props.comment.user.id && !this.state.editing &&
                                    <>
                                        <Level.Item>
                                            <Button small text onClick={this.toggleEditing}>
                                                <Icon><FontAwesomeIcon icon={'edit'} /></Icon> Edit
                                            </Button>
                                        </Level.Item>
                                        <Level.Item>
                                            <Button small text onClick={this.onDelete}>
                                                <Icon><FontAwesomeIcon icon={'trash'} /></Icon> Delete
                                            </Button>
                                        </Level.Item>
                                    </>
                                }
                            </Level.Left>
                        </Level>
                    </Content>
                </Media.Content>
            </Media>
        )
    }
}

export default withCurrentUser(Comment);