import React from 'react';
import {Hero, SubTitle, Button, Icon} from 'vendor/bulma';
import {Link} from 'react-router-dom';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {actions as editorActions} from "../../../../ducks/editor";
import { connect } from 'react-redux';

import {GlobalStream} from "features/stream";

const mapDispatchToProps = (dispatch) => ({
    toggleEditor: () => dispatch(editorActions.toggleEditor())
})

const CtaButton = connect(
    null,
    mapDispatchToProps
)(
    (props) => (
        <Button onClick={props.toggleEditor} className={"is-medium is-primary CtaButton"}>
            <Icon>
                <FontAwesomeIcon icon={'plus-square'} />
            </Icon>
            <span>Add your first task</span>
        </Button>
    )
)

const NoActivityCard = (props) => (
    <div className={"NoActivityCard"}>
        <Hero className={"has-text-centered"}>
            <Hero.Body>
                <SubTitle is='3'>
                    <strong className="has-text-grey-dark">Your log is empty.</strong>
                    <SubTitle is='4'>
                        <span className="has-text-grey-light">Click an action below, or keep scrolling to discover Makerlog.</span>
                    </SubTitle>
                    <p>
                        <CtaButton />
                    </p>
                    <p>
                        <Link to={'/explore/makers'} className={"button"}>
                            <Icon>
                                <FontAwesomeIcon icon={'users'} />
                            </Icon>
                            <span>Explore makers</span>
                        </Link>
                        &nbsp;
                        <Link to={'/apps'} className={"button"}>
                            <Icon>
                                <FontAwesomeIcon icon={'plug'} />
                            </Icon>
                            <span>Discover apps & integrations</span>
                        </Link>
                    </p>
                </SubTitle>
            </Hero.Body>
        </Hero>
        <GlobalStream />
    </div>
);

export default NoActivityCard;