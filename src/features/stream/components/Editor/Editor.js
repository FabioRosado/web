import React from 'react';
import { connect } from 'react-redux';
import { actions as editorActions } from 'ducks/editor';
import ErrorMessageList from "components/forms/ErrorMessageList";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {Box, Button, Icon, Image, SubTitle, Table, Tag} from "vendor/bulma";
import processString from "react-process-string";
import ProjectLink from "components/ProjectLink";
import Modal from "components/Modal";
import {HotKeys} from "react-hotkeys";
import Dropzone from 'react-dropzone'

const BasicTask = ({ task, onDelete }) => {
    //let doneIcon = faCheck;
    //let remainingIcon = faDotCircle;

    let icon = (task.done) ?
        <FontAwesomeIcon icon={'check-circle'} color="#27ae60" />
        :
        <FontAwesomeIcon icon={'dot-circle'} color="#f39c12" />;

    const hashtagConfig = {
        regex: /#([a-z0-9_-]+?)( |,|$|\.)/gim,
        fn: (key, result) => {
            let projectName = result[1];

            return (
                <ProjectLink key={result[1]}>#{projectName}</ProjectLink>
            )
        }
    }

    let processed = processString([hashtagConfig])(task.content);

    return (
        <div className={"entry " + (task.done ? "done" : (task.in_progress ? 'in_progress' : "remaining"))}>
            {icon} &nbsp; {processed} <Button className={"button is-text is-small"}  onClick={onDelete}><FontAwesomeIcon icon={'trash'} /></Button> {task.attachment && <FontAwesomeIcon icon={'camera'} />}
        </div>
    )
}

const HelpBox = ({ onClose }) => (
    <Box>
        <Table bordered fullwidth>
            <Table.Body>
                <Table.Tr>
                    <Table.Th>Key</Table.Th>
                    <Table.Th>Action</Table.Th>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Shift + N</Tag>
                    </Table.Td>
                    <Table.Td>
                        Open Editor.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Enter</Tag>
                    </Table.Td>
                    <Table.Td>
                        Appends current task to queue.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Ctrl/Cmd + Enter</Tag>
                    </Table.Td>
                    <Table.Td>
                        Posts all tasks in queue.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Ctrl/Cmd + D</Tag>
                    </Table.Td>
                    <Table.Td>
                        Toggle done of current task.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Ctrl/Cmd + I</Tag>
                    </Table.Td>
                    <Table.Td>
                        Toggle open image uploader.
                    </Table.Td>
                </Table.Tr>
            </Table.Body>
        </Table>
        <Button text onClick={onClose}>Close</Button>
    </Box>
)

class Editor extends React.Component {
    state = {
        showHelpCard: false,
        showImageUploader: false,
        typeTick: 0,
    }

    ticks = 0

    handlers = () => ({
        'enter': () => {
            if (this.props.editorValue.length <= 3) {
                return false
            }

            this.props.addToQueue();
        },
        'mod+enter': this.onSubmit,
        'mod+i': this.toggleImageUploader,
        'mod+d': () => {
            this.handleTypeChange()
            return false
        }
    })

    changeTick = () => {
        let newTick = this.state.typeTick + 1
        // three is number of changes possible (todo, inprogress, remaining)
        // reset to done if not
        this.setState({
            typeTick: (newTick <= 3 ? newTick : 0)
        })
    }

    handleTypeChange = async () => {
        // three is number of changes possible (todo, inprogress, remaining)
        // reset to done if not
        let newTick = this.state.typeTick + 1
        // three is number of changes possible (todo, inprogress, remaining)
        // reset to done if not
        await this.setState({
            typeTick: (newTick <= 2 ? newTick : 0)
        })
    }

    renderIcon = () => {
        let doneIcon = 'check';
        let remainingIcon = 'dot-circle';

        return (this.props.editorDone) ?
            <FontAwesomeIcon size='2x' icon={doneIcon} color={'white'} />
            :
            <FontAwesomeIcon size='2x' icon={remainingIcon} color={'white'} />
    }

    onDrop = (acceptedFiles, rejectedFiles) => {
        const file  = acceptedFiles[0];
        this.props.setEditorAttachment(file)
    }

    onClickIcon = () => {
        this.handleTypeChange()
    }

    onSubmit = () => {
        if (this.props.editorValue.length >= 3) {
            this.props.addToQueue()
        }
        this.props.createTasks();
    }

    renderQueue = () => {
        return this.props.queue.map(
            t => <BasicTask task={t} onDelete={() => this.props.removeFromQueue(t)} />
        )
    }

    toggleHelpCard = () => {
        this.setState({ showHelpCard: !this.state.showHelpCard })
    }

    toggleImageUploader = () => {
        this.setState({ showImageUploader: !this.state.showImageUploader })
    }

    render() {
        switch (this.state.typeTick) {
            case 0:
                this.props.markDone()
                break;

            case 1:
                this.props.markRemaining()
                break;

            case 2:
                this.props.markInProgress()
                break;

            default:
                this.props.markDone()
        }

        return (
            <Modal
                open={this.props.open}
                onClose={this.props.onClose}
                background={'transparent'}>
                <div className={"Editor-container"}>
                    <section className={"Editor"}>
                        <div onClick={this.onClickIcon} className={"icon" + (this.props.editorDone ? ' done' : (this.props.editorInProgress ? ' in_progress' : ' todo'))}>
                            {this.renderIcon()}
                        </div>
                        <HotKeys handlers={this.handlers()} className={"input-div"}>
                            <input
                                id={"editor-input"}
                                autoFocus
                                value={this.props.editorValue}
                                onChange={e => this.props.setEditorValue(e.target.value)}
                                autocomplete={"off"}
                                placeholder={(
                                    this.props.editorDone ? "Write something you did today." :
                                        (this.props.editorInProgress ? "Write what you're working on." : "Write a to-do.")
                                )} />
                        </HotKeys>
                        <div onClick={this.toggleImageUploader} className={"icon uploader"}>
                            <FontAwesomeIcon icon={'camera'} />
                        </div>
                    </section>
                    {
                        this.state.showImageUploader &&
                        <Box>
                            <Dropzone className={"dropzone"} accept="image/*" multiple={false} onDrop={this.onDrop}>
                                <SubTitle>Drop an image to attach here</SubTitle>
                                {this.props.editorAttachment &&
                                    <Image src={this.props.editorAttachment.preview} />
                                }
                            </Dropzone>
                        </Box>
                    }
                    <Box className={this.props.queue.length === 0 && "is-hidden-mobile"}>
                        {this.props.createFailed &&
                            <ErrorMessageList errorMessages={this.props.errorMessages} fieldErrors={this.props.fieldErrors} />
                        }
                        {this.props.queue.length > 0 ?
                            this.renderQueue()
                            :
                            <small className={"has-text-grey"}>
                                <strong>Click on the icon to change task type between done, to-do, or in progress.</strong>
                                <br />
                                {
                                    // eslint-disable-next-line
                                } <Tag>Enter</Tag> adds another task. Press <Tag>Cmd/Ctrl + Enter</Tag> to finish. <a onClick={this.toggleHelpCard}>Shortcuts &raquo;</a>
                            </small>
                        }
                    </Box>
                    {this.state.showHelpCard &&
                        <HelpBox onClose={this.toggleHelpCard} />
                    }
                    {
                        (this.props.editorValue.length > 0 || this.props.queue.length > 0) ?
                            <div>
                                <Button loading={this.props.isCreating} onClick={this.onSubmit} className={'button is-medium is-primary is-rounded'}>
                                    <Icon medium><FontAwesomeIcon icon={'pencil-alt'} /></Icon> Submit
                                </Button>
                            </div>
                            :
                            null
                    }
                </div>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => ({
    open: state.editor.open,
    queue: state.editor.queue,
    editorAttachment: state.editor.editorAttachment,
    isCreating: state.editor.isCreating,
    editorValue: state.editor.editorValue,
    editorDone: state.editor.editorDone,
    editorInProgress: state.editor.editorInProgress,
    createFailed: state.editor.createFailed,
    errorMessages: state.editor.errorMessages,
    fieldErrors: state.editor.fieldErrors,
})

const mapDispatchToProps = (dispatch) => ({
    onClose: () => dispatch(editorActions.toggleEditor()),
    addToQueue: () => dispatch(editorActions.addToQueue()),
    removeFromQueue: (t) => dispatch(editorActions.removeFromQueue(t)),
    createTasks: () => dispatch(editorActions.createTasks()),
    setEditorValue: (v) => dispatch(editorActions.setEditorValue(v)),
    toggleEditorDone: () => dispatch(editorActions.toggleEditorDone()),
    setEditorAttachment: (a) => dispatch(editorActions.setEditorAttachment(a)),
    markDone: () => dispatch(editorActions.markDone()),
    markInProgress: () => dispatch(editorActions.markInProgress()),
    markRemaining: () => dispatch(editorActions.markRemaining()),
})


Editor.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Editor);