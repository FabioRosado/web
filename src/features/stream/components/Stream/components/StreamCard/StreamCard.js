import React from 'react';
import PropTypes from 'prop-types';
import { uniqBy } from "lodash-es";
import {Content, Level} from 'vendor/bulma';
import { groupTasksByDone, orderByDate } from "lib/utils/tasks";
import EntryList from "../../../EntryList";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import withCurrentUser from "../../../../../users/containers/withCurrentUser";
import {Tooltip} from "react-tippy";
import { StreamCard as Card } from './styled';
import { UserMedia } from "features/users";
import {UserBadges} from "../../../../../../components/badges";

class StreamCard extends React.Component {
    state = {
        shareBarOpen: false,
    }

    getUser = () => {
        return this.props.userActivity[0].user;
    }

    getProjects = () => {
        // collect task projects arrays into one.
        let projects =  this.props.userActivity.map(
            a => a.project_set
        ).reduce(
            (a, b) => a.concat(b),
            []
        );
        // clean up dupes.
        return uniqBy(projects, (p) => p.id);
    }

    getTasks = () => {
        return groupTasksByDone(this.props.userActivity)
    }

    generateTweetText = (doneTasks) => {
        let name = this.getUser().twitter_handle ? `@${this.getUser().twitter_handle}` : this.getUser().username;
        let text =  `Done today by ${name} on @GetMakerlog:\n`;

        if (this.props.me.id === this.getUser().id) {
            text =  `Done today on @GetMakerlog:\n`;
        }

        orderByDate(doneTasks, 'asc').map(task => {
            text = text + `\n✅ ${task.content}`;
            return true;
        })

        return text
    }

    render() {
        let tasks = this.getTasks();
        let user = this.getUser();

        return (
            <Card highlighted={user.id === this.props.me.id || user.gold} accent={user.accent}>
                <Card.Header>
                    <Level style={{marginBottom: 0}} mobile>
                        <Level.Left>
                            <Level.Item>
                                <UserMedia user={user} />
                            </Level.Item>
                        </Level.Left>
                        <Level.Right>
                            <Level.Item className={"is-hidden-mobile"}>
                                <UserBadges user={user} />
                            </Level.Item>
                            <Level.Item>
                                <Tooltip
                                    interactive
                                    useContext
                                    html={
                                        <span>Tweet these tasks</span>
                                    }
                                    position={'top'}
                                    size={'small'}>
                                    <a className="twitter-share-button"
                                       target={'_blank'}
                                       href={`https://twitter.com/intent/tweet?text=${encodeURIComponent(this.generateTweetText(orderByDate(tasks.done)))}`}>
                                        <FontAwesomeIcon icon={['fab', 'twitter']} color={"lightgray"} />
                                    </a>
                                </Tooltip>
                            </Level.Item>
                            {/*

                            <Level.Item>
                                <FontAwesomeIcon icon={"ellipsis-v"} color={"lightgray"} />
                            </Level.Item>
                             */}
                        </Level.Right>
                    </Level>
                </Card.Header>

                <Card.Content>
                    <Content>
                        {tasks.in_progress &&
                        <div>
                            <EntryList tasks={orderByDate(tasks.in_progress)} />
                        </div>}
                        {tasks.done &&
                        <div>
                            <EntryList tasks={orderByDate(tasks.done)} />
                        </div>}
                        {tasks.remaining &&
                        <div>
                            <EntryList tasks={orderByDate(tasks.remaining)} />
                        </div>}
                    </Content>
                </Card.Content>
            </Card>
        )
    }
}

StreamCard.propTypes = {
    userActivity: PropTypes.array.isRequired
}

export default withCurrentUser(StreamCard);