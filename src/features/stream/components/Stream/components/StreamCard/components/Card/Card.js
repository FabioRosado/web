import React from 'react';
import { Box } from 'vendor/bulma';

const Card = (props) => (
    <Box className={props.className}>
        {props.children}
    </Box>
);

export default Card;