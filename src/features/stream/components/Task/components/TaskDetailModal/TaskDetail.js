import React from 'react';
import {connect} from 'react-redux';
import { Button, Icon, Level } from "vendor/bulma";
import Task from "../../Task";
import {RIEInput} from "riek";
import {actions as tasksActions} from "ducks/tasks";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {actions as streamActions} from "ducks/stream";
import Embed from 'components/Embed';
import config from '../../../../../../config';
import ShareBar from "../../../../../../components/ShareBar";
import { StreamCard as Card } from "../../../Stream/components/StreamCard/styled";
import {Dropdown} from "components/Dropdown";
import {Control, Field, Input} from "../../../../../../vendor/bulma";
import { UserMedia } from 'features/users';

class TaskDetail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            initialContent: this.props.task.content,
            embedOpen: false,
            confirmDelete: false,
            marking: false,
            editing: false,
            editingPosting: false,
            editingFailed: false,
        }
    }

    toggleEditing = () => {
        this.setState({
            editing: !this.state.editing
        })
    }

    renderEditingState = () => (
        <Field>
            <Control>
                <Input
                    value={this.state.initialContent}
                    onKeyDown={e => {
                        if (e.keyCode === 13) {
                            this.onEdit({ content: this.state.initialContent })
                            this.setState({ editing: false })
                        }
                    }}
                    onChange={e => {
                        this.setState({ initialContent: e.target.value })
                    }} />
            </Control>
        </Field>
    )

    onChangeStatus = () => {
        this.setState({
            marking: !this.state.marking
        })
    }

    getPermalink = () => {
        return `${config.BASE_URL}/tasks/${this.props.task.id}`
    }

    generateTweetText = () => {
       return `✅ ${this.props.task.content} \n ${this.getPermalink()}`;
    }

    toggleEmbed = () => {
        this.setState({
            embedOpen: !this.state.embedOpen
        })
    }

    onEdit = (payload) => {
        this.props.updateTask(this.props.task.id, payload)
    }

    onTryDelete = () => {
        if (this.state.confirmDelete) {
            this.onDelete()
        }

        this.setState({ confirmDelete: true })
    }

    onDelete = () => {
        this.props.deleteTask(this.props.task.id)
        this.props.removeFromStream(this.props.task.id)
    }

    renderActionBar = () => {
        if (this.props.isLoggedIn && this.props.me.id === this.props.task.user.id) {
            return (
                <ShareBar
                    compact
                    rightAlignShare
                    tweetText={this.generateTweetText()}
                    permalink={this.getPermalink()}
                    extraItemsFirst={() => (
                        <>
                            <Level.Item>
                                <Dropdown trigger={() => (
                                    <Button text small>
                                        <Icon>
                                            <FontAwesomeIcon icon={'check'} size={'sm'} />
                                        </Icon> {this.state.marking ? 'Marking...' : 'Mark as...'}
                                    </Button>
                                )}>
                                    {this.props.task.done &&
                                        <>
                                            <a
                                                onClick={() => {
                                                    this.onChangeStatus();
                                                    this.props.markRemaining(this.props.task.id)
                                                }}
                                                href={"#mark-remaining"}
                                                className={"dropdown-item"}>
                                                <Icon><FontAwesomeIcon icon={'dot-circle'} /></Icon> Mark remaining
                                            </a>
                                            <a
                                                onClick={() => {
                                                    this.onChangeStatus()
                                                    this.props.markInProgress(this.props.task.id)
                                                }}
                                                href={"#mark-in-progress"}
                                                className={"dropdown-item"}>
                                                <Icon><FontAwesomeIcon icon={'dot-circle'} /></Icon> Mark in progress
                                            </a>
                                        </>
                                    }
                                    {!this.props.task.done &&
                                    <>
                                        <a
                                            onClick={() => {
                                                this.onChangeStatus()
                                                this.props.markDone(this.props.task.id)
                                            }}
                                            href={"#mark-remaining"}
                                            className={"dropdown-item"}>
                                            <Icon><FontAwesomeIcon icon={'check-circle'} /></Icon> Mark done
                                        </a>
                                    </>
                                    }
                                </Dropdown>
                            </Level.Item>
                            <Level.Item>
                                <Button text small onClick={this.toggleEditing}>
                                    <Icon>
                                        <FontAwesomeIcon icon={'edit'} size={'sm'} />
                                    </Icon> Edit
                                </Button>
                            </Level.Item>
                        </>
                    )}
                    extraItemsLeft={() => (
                       <>
                           <Level.Item>
                               <Button small text={!this.state.confirmDelete} onClick={this.onTryDelete} danger={this.state.confirmDelete}>
                                   <Icon>
                                       <FontAwesomeIcon icon={'trash'} />
                                   </Icon> {!this.state.confirmDelete && 'Delete'}
                                   {this.state.confirmDelete &&
                                     <span>&nbsp;Click again to confirm.</span>
                                   }
                               </Button>
                           </Level.Item>
                        </>
                    )}
                    extraItemsRight={() => (
                        <>
                            <Level.Item>
                                <Button text small onClick={this.toggleEmbed}>
                                    <Icon>
                                        <FontAwesomeIcon icon={'code'} size={'sm'} />
                                    </Icon>
                                </Button>
                            </Level.Item>
                        </>
                    )}
                />
            )
        } else {
            return (
                <ShareBar
                    tweetText={this.generateTweetText()}
                    permalink={this.getPermalink()}
                    extraItemsLeft={() => (
                        <Level.Item>
                            <Button text small onClick={this.toggleEmbed}>
                                <Icon>
                                    <FontAwesomeIcon icon={'code'} size={'sm'} />
                                </Icon> Embed
                            </Button>
                        </Level.Item>
                    )}
                />
            )
        }
    }

    render() {
        const user = this.props.task.user;

        return (
            <Card highlighted={(this.props.me ? (user.id === this.props.me.id) : false) || user.gold} accent={user.accent}>
                <Card.Header>
                    <UserMedia user={this.props.task.user} />
                </Card.Header>

                <Card.Content>
                    {this.state.editing ?
                        this.renderEditingState()
                        :
                        (
                            this.props.me.id === this.props.task.user.id ?
                                <Task
                                    task={this.props.task}
                                    withDetailModal={false}
                                    withTooltip={false}
                                    contentWrapper={() => (
                                        <RIEInput
                                            value={this.props.task.content}
                                            className={'editing'}
                                            propName={'content'}
                                            change={this.onEdit} />
                                    )}
                                />
                                :
                                <Task
                                    task={this.props.task}
                                    withDetailModal={false}
                                    withTooltip={false}
                                />
                        )
                    }
                </Card.Content>
                <Card.Footer>
                    <div style={{width: "100%"}}>
                        {this.renderActionBar()}

                        {this.state.embedOpen &&
                            <div style={{width: "50%"}}>
                                <br />
                                <Embed task url={`/tasks/${this.props.task.id}/embed`} />
                            </div>
                        }
                    </div>
                </Card.Footer>
            </Card>
        )
    }
}


const mapStateToProps = (state) => ({
    me: state.user.me,
    user: state.user.me,
    isLoggedIn: state.auth.loggedIn,
})

export default connect(
    mapStateToProps,
    (dispatch) => ({
        updateTask: (id, payload) => {
            dispatch(tasksActions.updateTask(id, payload))
        },
        deleteTask: (id) => dispatch(tasksActions.deleteTask(id)),
        removeFromStream: (id) => dispatch(streamActions.removeTask(id)),
        markDone: (id) => dispatch(tasksActions.markDone(id)),
        markInProgress: (id) => dispatch(tasksActions.markInProgress(id)),
        markRemaining: (id) => dispatch(tasksActions.markRemaining(id)),
    })
)(TaskDetail)