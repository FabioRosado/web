import React from 'react';
import PropTypes from 'prop-types';
import Emoji from "components/Emoji";

const PraiseCount = ({ amount }) => {
    if (amount) {
        return (
            <span className={"PraiseCount has-text-grey-light"}>
                <Emoji emoji={"👏"} />{amount}
            </span>
        )
    } else {
        return null
    }
}

PraiseCount.propTypes = {
    amount: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object
    ]),
}

export default PraiseCount;