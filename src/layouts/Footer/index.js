import React from 'react';
import './style.css';
import {Link} from "react-router-dom";
import {Level} from "../../vendor/bulma";
import { withTheme } from 'styled-components';

const Footer = (props) => (
    <footer id="footer">
            <div className={"container"}>
                <div className="columns">
                    <div className="column has-vertically-aligned-content branded">
                        <div>
                            <a rel="noopener noreferrer" target="_blank" href="https://sergiomattei.com">
                                <img src={props.theme.isDark ? "https://sergiomattei.com/img/Arkus-Fatter.png" : "https://sergiomattei.com/img/Arkus-Fatter-Black.png"} alt="Sergio Mattei's Logo" />
                            </a>
                        </div>
                    </div>

                    <div className={"column menu"}>
                        <Level>
                            <Level.Right>
                                <Level.Item centered>
                                    <Link to={'/about'}>
                                        About
                                    </Link>
                                </Level.Item>

                                <Level.Item></Level.Item>

                                <Level.Item>
                                    <Link to={'/open'}>
                                        Open
                                    </Link>
                                </Level.Item>

                                <Level.Item></Level.Item>

                                <Level.Item>
                                    <Link to={'/migrate'}>
                                        Migrate
                                    </Link>
                                </Level.Item>

                                <Level.Item></Level.Item>

                                <Level.Item>
                                    <a href="https://give.getmakerlog.com" target="_blank" rel="noopener noreferrer">
                                        Give
                                    </a>
                                </Level.Item>

                                <Level.Item></Level.Item>

                                <Level.Item>
                                    <a href="https://ideas.getmakerlog.com" target="_blank" rel="noopener noreferrer">
                                        Feedback
                                    </a>
                                </Level.Item>

                                <Level.Item></Level.Item>

                                <Level.Item>
                                    <a href="https://bugs.getmakerlog.com" target="_blank" rel="noopener noreferrer">
                                        Bugs
                                    </a>
                                </Level.Item>

                                <Level.Item></Level.Item>

                                <Level.Item>
                                    <a href="https://twitter.com/getmakerlog" target="_blank" rel="noopener noreferrer">
                                        Twitter
                                    </a>
                                </Level.Item>

                                <Level.Item></Level.Item>

                                <Level.Item>
                                    <a href="https://medium.com/makerlog" target="_blank" rel="noopener noreferrer">
                                        Blog
                                    </a>
                                </Level.Item>
                            </Level.Right>
                        </Level>
                    </div>
                </div>
            </div>
    </footer>
);

export default withTheme(Footer);