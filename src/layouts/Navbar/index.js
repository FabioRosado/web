import React from 'react';
import { connect } from 'react-redux';
import { actions as authActions } from 'ducks/auth';
import {NavLink, withRouter} from 'react-router-dom';
import './style.css';
import LocalOnly from "containers/LocalOnly";
import styled from 'styled-components';
import LoggedOutMenu from "./components/LoggedOutMenu";
import LoggedInMenu from "./components/LoggedInMenu";
import Spinner from "components/Spinner";

// background color set by is-dark
const Navigation = styled.nav`
    background-color: #303030;
    height: 60px;
    z-index: 100;

    .navbar-item {
        -webkit-transition: all 0.4s ease;
        transition: all 0.4s ease;
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
        font-size: 0.9rem;
    }

    .navbar-item.is-active, .navbar-item.is-located > .navbar-link {
        color: ${props => props.theme.primaryDarker} !important;
        background-color: #292929 !important;
    }
    
    .navbar-dropdown {
        box-shadow: 6px 2px 64px -16px rgba(0,0,0,0.75) !important;
    }
   
    .navbar-dropdown > .navbar-item.is-active {
      color: white !important;
      background-color: ${props => props.theme.primaryDarker} !important;
    }
    
    .navbar-dropdown > .navbar-item:hover {
      background-color: ${props => props.theme.primaryDarker};
      color: white;
    }

    .brand { 
        z-index: 101;
        position: absolute;
        text-align: center;
        margin-left: -60px;
        left: 50%;
        height: 100%
        display: flex
        align-items: center;
        height: 60px;
        font-size: 1.3rem;
        color: ${props => props.theme.primary} !important;
        font-weight: bold;

        font-family: "Poppins", sans-serif;
    }

    .brand:hover {
        background-color: #383838;
    }
    
    .navbar-burger {
      background-color: transparent;
      border: none;
      height: 60px;
    }
    
    .navbar-burger span {
      background-color: white;
    }

    @media screen and (max-width: 1200px) {
        .navbar-brand .brand {
            position: relative !important;
            margin-left: 1rem !important;
            left: auto !important;
        }
    }
`;

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
        }
    }

    onToggleExpand = () => {
        if (this.state.expanded) {
            this.setState({ expanded: !this.state.expanded })
        }
    }

    render() {
        return (
            <Navigation className={"navbar is-dark " + (this.props.transparent ? "transparent-navbar" : "")  + (this.props.translucent ? "translucent-navbar" : "")} id="main-navbar">
                <div className="container">
                    <div className="navbar-brand">
                        <NavLink to={'/'} className="brand">
                            Makerlog <LocalOnly>Dev</LocalOnly> {this.props.isSyncing && <span style={{marginTop: 8, marginLeft: 3}}><Spinner small color={"white"} /></span>}
                        </NavLink>
                        <button className="button navbar-burger" onClick={e => this.setState({ expanded: !this.state.expanded })}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                    </div>

                    {!this.props.isLoggedIn &&
                        <LoggedOutMenu
                            expanded={this.state.expanded}
                            onToggleExpand={this.onToggleExpand}
                        />
                    }

                    {this.props.isLoggedIn &&
                        <LoggedInMenu
                            expanded={this.state.expanded}
                            onToggleExpand={this.onToggleExpand}
                            user={this.props.user}
                            isSyncing={this.props.isSyncing}
                            onClickLogout={this.props.onClickLogout}
                        />
                    }
                </div>
            </Navigation>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.loggedIn,
        user: state.user.me,
        isSyncing: state.tasks.isSyncing || state.projects.isSyncing,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickLogout: () => {
            dispatch(authActions.logout());
        }
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Navbar)
);