import React from 'react';
import {connect} from "react-redux";
import { actions as editorActions } from 'ducks/editor';
import {HotKeys} from "react-hotkeys";

class AppShortcuts extends React.Component {
    keyMap = {
        'toggleEditor': 'shift+n'
    }

    handlers = () => {
        return {
            'toggleEditor': () => {
                this.props.toggleEditor();

                return false;
            }
        }
    }

    render() {
        return <HotKeys keyMap={this.keyMap} handlers={this.handlers()} focused={true} attach={window} />
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: state.auth.loggedIn,
})

const mapDispatchToProps = (dispatch) => ({
    toggleEditor: () => dispatch(editorActions.toggleEditor())
})


AppShortcuts.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppShortcuts);