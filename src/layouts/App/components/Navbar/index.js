import React from 'react';
import { connect } from 'react-redux';
import { actions as authActions } from 'ducks/auth';
import { NavLink } from 'react-router-dom';
import LoggedInMenu from './components/LoggedInMenu';
import LoggedOutMenu from './components/LoggedOutMenu';
import './style.css';
import LocalOnly from "../../../../containers/LocalOnly";

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
        }
    }

    onClickCloseExpand = () => {
        if (this.state.expanded) {
            this.setState({ expanded: !this.state.expanded })
        }
    }

    render() {
        return (
            <nav className={"navbar navbar-dark is-dark is-bold"  + (this.props.transparent ? "transparent-navbar" : "")} aria-label="main navigation" id="main-navbar">
                <div className="container">
                    <div className="navbar-brand">
                        <NavLink to="/" className="navbar-item">
                            <h1 className="subtitle">
                                Makerlog
                                <LocalOnly>
                                    <span className="has-text-grey-lighter">
                                        &nbsp;Local
                                    </span>
                                </LocalOnly>
                            </h1>
                        </NavLink>
                        <button className="button navbar-burger" onClick={e => this.setState({ expanded: !this.state.expanded })}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                    </div>

                    {this.props.isLoggedIn &&
                        <LoggedInMenu
                            onClickLogout={this.props.onClickLogout}
                            user={this.props.user}
                            isSyncing={this.props.isSyncing}
                            isExpanded={this.state.expanded}
                            onClickCloseExpand={this.onClickCloseExpand}/>}

                    {!this.props.isLoggedIn &&
                        <LoggedOutMenu
                            isExpanded={this.state.expanded}
                            onClickCloseExpand={this.onClickCloseExpand}/>}
                </div>
            </nav>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.loggedIn,
        user: state.user.me,
        isSyncing: state.tasks.isSyncing || state.projects.isSyncing,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickLogout: () => {
            dispatch(authActions.logout());
        }
    }
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Navbar);