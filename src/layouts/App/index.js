import React from 'react';
import {Provider} from 'react-redux';
import { store } from 'store';
import App from './App';
import RehydrateToken from './components/RehydrateToken';

export default () => (
    <Provider store={store}>
        <div className={"App"}>
            <App />
            <RehydrateToken />
        </div>
    </Provider>
);