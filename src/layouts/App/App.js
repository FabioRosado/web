import React from 'react';
import { ConnectedRouter } from 'react-router-redux';
import {connect} from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { history, persistor } from 'store';
import Spinner from 'components/Spinner';
import routes from './routes';
import { actions as statsActions } from 'ducks/stats';
import { actions as tasksActions } from 'ducks/tasks';
import {actions as appActions} from 'ducks/app';
import { NotificationsView } from "features/notifications";
import { Editor } from "features/stream";
import AppShortcuts from "./components/AppShortcuts";
import Helmet, { HelmetProvider } from 'react-helmet-async';
import Colors from 'styles/colors';
import {ThemeProvider} from "styled-components";

class App extends React.Component {
    componentDidMount() {
        this.props.appInit();
    }

    componentWillUnmount() {
        this.exitDarkMode()
    }

    getColor = () => {
        if (!this.props.user || !this.props.user.gold) return null;

        return this.props.user.accent ? this.props.user.accent : null;
    }

    isDark = () => {
        if (!this.props.user || !this.props.user.gold) return false;

        return this.props.user.dark_mode ? this.props.user.dark_mode : false;
    }

    enterDarkMode = () => {
        if (!document.body.classList.contains("is-dark-mode")) {
            document.body.classList.add("is-dark-mode");
        }
    }

    exitDarkMode = () => {
        if (document.body.classList.contains("is-dark-mode")) {
            document.body.classList.remove("is-dark-mode");
        }
    }

    render() {
        const theme = this.getColor() ?
            new Colors(this.getColor(), this.isDark()) :
            new Colors();

        console.log(this.props.user)

        if (this.isDark()) {
            console.log("Dark enabled")
            this.enterDarkMode()
        } else {
            this.exitDarkMode()
        }

        return (
            <ThemeProvider theme={theme}>
                <PersistGate loading={<Spinner />} persistor={persistor}>
                    <ConnectedRouter history={history}>
                        <HelmetProvider>
                            <div>
                                <Helmet>
                                    <title>Makerlog {process.env.NODE_ENV === 'development' ? "(local)" : ""}</title>
                                    <meta name="description" content="Makerlog is the dead-simple task log that helps you stay productive and ship faster." />
                                </Helmet>

                                {routes}

                                <NotificationsView />
                                <Editor />

                                <AppShortcuts/>
                            </div>
                        </HelmetProvider>
                    </ConnectedRouter>
                </PersistGate>
            </ThemeProvider>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user.me ? state.user.me : null,
        isLoggedIn: state.auth.loggedIn,
        apiHealthy: state.app.healthy,
        feedbackOpen: state.app.feedbackOpen,
        token: state.auth.token,
        statsAlreadyLoaded: state.stats.ready,
        tasksAlreadyLoaded: state.tasks.ready,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadStats: (silently = false) => dispatch(statsActions.fetchStats(silently)),
        loadTasks: () => dispatch(tasksActions.loadTasks()),
        appInit: () => dispatch(appActions.appInit()),
        toggleFeedback: () => dispatch(appActions.toggleFeedback()),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);