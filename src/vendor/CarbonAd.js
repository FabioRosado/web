import React from 'react'
import {Level} from "./bulma";
import './CarbonAd.css';
import {StreamCard as Card} from "../features/stream/components/Stream/components/StreamCard/styled";
import OutboundLink from "../components/OutboundLink";

class CarbonAd extends React.Component {
    componentDidMount() {
        const s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        // s.id = '_carbonads_js';
        // s.src = `https://cdn.carbonads.com/carbon.js?serve=CK7DC5QJ&placement=${this.props.placement}`;
        s.src = `https://intravert.co/serve/0359e0d7b3.3.js`;
        this.instance.appendChild(s);
    }

    render() {
        return (
            <div>
            <Card style={{marginBottom: 0}}>
                <Card.Content>
                    <div  ref={el => (this.instance = el)}></div>
                    <div className="intravert-space" id={"space-0359e0d7b33"}></div>
                </Card.Content>
            </Card>
            <br />

            <div className={"intravert-card"}>
                <Level style={{width: "100%"}}>
                    <Level.Left>
                        Reach thousands of makers monthly.
                    </Level.Left>
                    <Level.Right>
                        <OutboundLink className={"button is-small is-primary is-rounded"} href={"https://intravert.co/book/0359e0d7b3/3/"}>Advertise here!</OutboundLink>
                    </Level.Right>
                </Level>
            </div>
            </div>
        )
    }
}


CarbonAd.defaultProps = {
    placement: 'getmakerlogcom'
}

export default CarbonAd;