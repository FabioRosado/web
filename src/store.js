import { createStore, applyMiddleware, compose } from 'redux';
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga';
import rootReducer from './ducks/rootReducer';
import rootSaga from './sagas';
import { googleAnalytics } from './vendor/reactGaMiddlewares';

import { persistStore } from 'redux-persist';


const initialState = {}
const enhancers = []


if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}


/*

Configure middleware for redux.

*/
const history = createHistory();
const grouterMid = routerMiddleware(history);

const sagaMiddleware = createSagaMiddleware()

const middleware = [
	grouterMid,
	sagaMiddleware,
]

if (process.env.NODE_ENV === 'production') {
    middleware.push(googleAnalytics)
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

/*

Create the store.

*/

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
)


/*

Create the persistor, and run the root saga.

*/
const persistor = persistStore(store)
sagaMiddleware.run(rootSaga)

if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
        module.hot.accept('./ducks/rootReducer', () => {
            const nextRootReducer = require('./ducks/rootReducer').default;
            store.replaceReducer(nextRootReducer);
        });
    }
}

export {
    store,
    persistor,
    history
}