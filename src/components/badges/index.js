import React from 'react';
import Tda from 'components/Tda';
import Streak from 'components/Streak';
import {Level, Tag} from "vendor/bulma";
import Emoji from "components/Emoji";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

export const Badge = (props) => (
    <span className={`Badge tag is-rounded ${props.className ? props.className : null}`} style={{backgroundColor: props.backgroundColor ? props.backgroundColor : null}}>
        {props.children}
    </span>
)

export const GoldBadge = (props) => (
    <Badge className={"is-dark"} backgroundColor={"rgb(229,193,0)"}>
        <FontAwesomeIcon icon={'check-circle'} color="white" />&nbsp;<strong>Gold</strong>
    </Badge>
)

export const DonorBadge = (props) => (
    <Badge className={"is-dark"}>
        <Emoji emoji={"✌️"} /> Donor
    </Badge>
)

export const TesterBadge = (props) => (
    <Badge className={"is-white"} backgroundColor={"#fdcb6e"}>
        <Emoji emoji={"🚧"} /> Tester
    </Badge>
)

export const StaffBadge = (props) => (
    <Badge className={"is-dark"}>
        <Emoji emoji={"👋"} /> Staff
    </Badge>
)

export const AwesomeBadge = (props) => (
    <Badge className={"is-dark"} backgroundColor={"#2d98da"}>
        <Emoji emoji={"💫"} /> Awesome
    </Badge>
)

export const HundredDayClubBadge = (props) => (
    <Badge className={"is-dark"} backgroundColor={"#ed776f"}>
        <span style={{textShadow: "0 0 10px white"}}><Emoji emoji={"💯"} /></span> 100 Day Club
    </Badge>
)

export const InlineLevel = (props) => (
    <Level style={{display: "inline-block", marginBottom: 0}} className={"is-inline-flex"}>
        {props.children}
    </Level>
)


export const UserBadges = ({ user }) => {
    return (
        <InlineLevel>
            {(user.username.toLowerCase() === 'booligoosh' || user.username.toLowerCase() === 'tomaswoksepp') && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <AwesomeBadge />
            </Level.Item>}
            {(user.streak >= 100) && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <HundredDayClubBadge />
            </Level.Item>}
            {user.is_staff && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <StaffBadge />
            </Level.Item>}
            {user.donor && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <DonorBadge />
            </Level.Item>}
            {user.tester && <Level.Item style={{paddingRight: 2, paddingLeft: 2, marginRight: 0}}>
                <TesterBadge />
            </Level.Item>}
        </InlineLevel>
    )
}


export const StatBar = ({ user, showUsername=false }) => (
    <div>
        {showUsername && <small>@{user.username}</small>}&nbsp;
        <Tag className={"is-rounded"}><Streak days={user.streak} /></Tag>&nbsp;
        <Tag className={"is-rounded"}><Tda tda={user.week_tda} /></Tag>
        <UserBadges user={user} />
    </div>
)

const StyledStreakBadge = styled(Tag)`
  color: white !important;
  background-color: ${props => props.user.streak > 0 ? '#E48845' : '#f5f5f5'} !important;
`


export const StreakBadge = ({ user }) => (
    <StyledStreakBadge className={"is-rounded"} user={user}>
        <Streak days={user.streak} />
    </StyledStreakBadge>
)

export const StatBadges = ({user}) => (
    <small>
        <div className="tags has-addons">
            {user.streak > 0 && <StreakBadge user={user} />}
            <Tag className={"is-rounded is-dark"} style={{backgroundColor: '#5A5A5A'}}>
                <Tda tda={user.week_tda} />
            </Tag>
        </div>
    </small>
)