import React from 'react';
import ClimbingBoxLoader from 'vendor/ClimbingBoxLoader';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SmallSpinner = styled.div`
    display: inline-block;
    position: relative;
    height: 30px;
    width: 30px;
    
    & div {
      margin: 0 auto;
      box-sizing: border-box;
      display: block;
      position: absolute;
      width: 15px;
      height: 15px;
      -webkit-transform-style: preserve-3d; 
      -webkit-transform: rotateZ(2deg);
      border: 1.5px solid ${props => props.color};
      border-radius: 50%;
      animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
      border-color: ${props => props.color} transparent transparent transparent;
      margin: 7px;
    }
    & div:nth-child(1) {
      animation-delay: -0.45s;
    }
    & div:nth-child(2) {
      animation-delay: -0.3s;
    }
    & div:nth-child(3) {
      animation-delay: -0.15s;
    }
    @keyframes lds-ring {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }
`

const Spinner = ({ color = "#2F2F2F", small = false, text = null }) => {
    if (small) {
        return (
            <center>
                <SmallSpinner color={color}>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </SmallSpinner>
            </center>
        );
    } else {
        return (
            <center>
                <ClimbingBoxLoader color={color} />
                <br />
                <small style={{ color: color }}>
                    {text}
                </small>
            </center>
        )
    }
}

Spinner.propTypes = {
    color: PropTypes.string,
    small: PropTypes.bool,
}

export default Spinner;