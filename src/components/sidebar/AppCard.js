import React from 'react';
import {Card, Level, Media} from "../../vendor/bulma";
import Emoji from "../Emoji";
import OutboundLink from "../OutboundLink";
import { UserContainer } from "features/users";
import Avatar from "../../features/users/components/Avatar/Avatar";

export default () => (
    <Card>
        <Card.Content>
            <Media>
                <Media.Content>
                    <p className="heading"><Emoji emoji="🛠"/> Spotlight</p>
                    <p style={{fontSize: 14}}>
                        Use the new Telegram bot and log straight from your messaging app!
                    </p>
                    <br />
                    <p>
                        <OutboundLink className="button is-primary is-small is-rounded" href="https://t.me/makerlog">
                            Join the Telegram
                        </OutboundLink>
                    </p>
                </Media.Content>
                <Media.Right>
                    <img alt="app screenshot" src="https://i.imgur.com/fncZ2rw.png" width="75" style={{borderRadius: 5}} />
                </Media.Right>
            </Media>
        </Card.Content>
    </Card>
)