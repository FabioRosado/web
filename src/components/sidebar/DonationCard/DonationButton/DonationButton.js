import React from 'react';

const DonationButton = (props) => (
    <a
        className = "bmc-button"
        target = "_blank"
        rel="noopener noreferrer"
        href = "https://give.getmakerlog.com/"> <img
        src = "https://i.imgur.com/ZS8iEhp.png"
        alt = "Give" />
        <span>Donate to Makerlog</span>
    </a>
)

DonationButton.propTypes = {}

export default DonationButton;