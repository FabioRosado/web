import React from 'react';
import {TrendingDiscussionList} from "features/discussions";
import {Card} from "../../vendor/bulma";
import Emoji from "../Emoji";

export default () => (
    <Card>
        <Card.Content>
            <p className="heading"><Emoji emoji="💬 "/> Trending discussions</p>
            <TrendingDiscussionList />
        </Card.Content>
    </Card>
)