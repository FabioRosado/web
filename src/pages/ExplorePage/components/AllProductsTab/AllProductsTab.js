import React from 'react';
import {Card, Container, Hero, SubTitle, Title} from 'vendor/bulma';
import LoggedOutMessage from "../../../../components/LoggedOutMessage";
import ProductsContainer from "../../../../features/products/containers/ProductsContainer";
import {ProductList} from "features/products";

const LaunchedProductsList = (props) => (
    <ProductList grid card {...props} />
)

class AllProductsTab extends React.Component {
    render() {
        return (
            <div className="AllProductsTab">
                <Hero className={"ProductHero"}>
                    <Hero.Body>
                        <Container>
                            <Title is="4" className={"has-text-white"}>
                                Recently launched
                            </Title>
                            <ProductsContainer recentlyLaunched component={LaunchedProductsList} />
                        </Container>
                    </Hero.Body>
                </Hero>
                <Container>
                    <LoggedOutMessage />
                    <br />
                    <SubTitle is={"4"}>
                        <strong>All products</strong>
                    </SubTitle>
                    <Card>
                        <Card.Content>
                            <ProductsContainer all component={(props) => <ProductList media medium {...props} />} />
                        </Card.Content>
                    </Card>
                </Container>
            </div>
        )
    }
}

AllProductsTab.propTypes = {}

export default AllProductsTab;