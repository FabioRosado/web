import React from 'react';
import {Card, Title} from 'vendor/bulma';
import {UserHero} from "features/users";
import {Leaderboards} from "features/stats";

const MakersTab = (props) => (
    <div className="MakersTab container">
        {props.topUsers.length !== 0 && <UserHero featured={true} user={props.topUsers[0]} />}
        <br />
        <Title is={"2"}>
            Leaderboards
        </Title>
        <Card>
            <Card.Content>
                <Leaderboards topUsers={props.topUsers} />
            </Card.Content>
        </Card>
    </div>
);

MakersTab.propTypes = {}

export default MakersTab;