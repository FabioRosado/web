import React from 'react';
import {Card, Container, SubTitle, Media, Button} from "vendor/bulma";
import Sticky from 'react-stickynode';
import {ActivityCard} from "features/stats";
import LoggedOutMessage from 'components/LoggedOutMessage';
import { getPopularTasks } from 'lib/stats';
import Spinner from '../../../../components/Spinner';
import {PeopleCard, Avatar} from 'features/users';
import {Entry} from 'features/stream';
import AdCard from "../../../../components/sidebar/AdCard";


class PopularTaskList extends React.Component {
    state = {
        loading: true,
        tasks: null,
        failed: false,
    }

    componentDidMount() {
        this.fetchPopularTasks()
    }


    fetchPopularTasks = async () => {
        this.setState({
            loading: true
        })
        try {
            const tasks = await getPopularTasks();
            this.setState({
                loading: false,
                tasks: tasks,
                failed: false,
            })
        } catch (e) {
            this.setState({
                loading: false,
                tasks: null,
                failed: true,
            })
        }
    }

    render() {
        if (this.state.loading && this.state.tasks === null) {
            return <Spinner />
        }

        if (this.state.failed) {
            return <strong>Failed to load tasks. <Button onClick={this.fetchPopularTasks}>Retry</Button></strong>
        }

        return (
            <div>
                <SubTitle is='4'>
                    <strong>Trending today</strong>
                </SubTitle>
                <Card>
                    <Card.Content>
                        {this.state.tasks.map(t => (
                            <Media key={t.id}>
                                <Media.Left>
                                    <Avatar user={t.user} is={32} />
                                </Media.Left>
                                <Media.Content>
                                    <Entry task={t} />
                                </Media.Content>
                            </Media>
                        ))}
                    </Card.Content>
                </Card>
            </div>
        );
    }
}


class PopularTab extends React.Component {

    render() {
        const props = this.props;
        
        return (
            <div>
                <Container className={"PopularTab"}>
                <br />
                <LoggedOutMessage />
                <div className={"columns"}>
                    <div className={"column"}>
                        <PopularTaskList />
                    </div>
                    <div className={"column sidebar is-hidden-mobile"} style={{marginTop: 54}}>
                        <Sticky enabled={true} top={30}>
                            <ActivityCard trend={props.worldStats.activity_trend} heading={'World pulse'} />
                            <br />
                            <PeopleCard worldStats={props.worldStats} />
                            <hr />
                            <AdCard />
                        </Sticky>
                    </div>
                </div>
            </Container>
            </div>
        );
    }
}

PopularTab.propTypes = {}

export default PopularTab;