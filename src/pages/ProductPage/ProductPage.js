import React from 'react';
import Spinner from "../../components/Spinner";
import {Redirect} from "react-router-dom";
import {Button, Container, Title} from "vendor/bulma";
import {getProductBySlug} from "../../lib/products";
import {Product} from "features/products";
import { ProductStream } from "features/stream";
import Helmet from 'react-helmet-async';
import Page from 'layouts/Page';

// Make ProductStream

class ProductPage extends React.Component {
    state = {
        isLoading: false,
        product: null,
        failed: false,
        notFound: false,
    }

    getProduct = async () => {
        this.setState({ failed: false, notFound: false, isLoading: true })
        try {
            const product = await getProductBySlug(this.props.match.params.slug);
            this.setState({ product: product, isLoading: false })
        } catch (e) {
            if (e.status_code && e.status_code === 404) {
                this.setState({ failed: true, notFound: true })
            } else {
                this.setState({ failed: true })
            }
        }
    }

    componentDidMount() {
        this.getProduct()
    }

    render() {
        if (this.state.notFound) {
            return <Redirect to="/404" />
        }

        if (this.state.isLoading) {
            return (
                <Page loading={true} />
            )
        }

        if (this.state.failed) {
            return (
                <center>
                    <Title>
                        Oops! There was a network error. <Button text onClick={this.getProduct}>Retry</Button>
                    </Title>
                </center>
            )
        }

        if (!this.state.failed && !this.state.isLoading && this.state.product) {
            return (
                <Page contained={false} className="ProductPage">
                    <Helmet>
                        <title>{this.state.product.name} | Makerlog</title>
                        <meta name="description" content={`${this.state.product.name} is built on Makerlog, the world's most supportive community of makers shipping together.`} />
                        <meta name="twitter:card" content={'summary'} />
                        <meta name="twitter:site" content="@getmakerlog" />
                        <meta name="twitter:title" content={`${this.state.product.name} on Makerlog`} />
                        <meta name="twitter:description" content={this.state.product.description ? this.state.product.description : `${this.state.product.name} is built on Makerlog, the world's most supportive community of makers shipping together.`} />
                        {this.state.product.icon &&
                            <meta name="twitter:image" content={this.state.product.icon} />
                        }
                    </Helmet>
                    <Product hero product={this.state.product} />
                    <br />
                    <Container>
                        <ProductStream productSlug={this.state.product.slug} />
                    </Container>
                </Page>
            )
        } else {
            return (
                <Page>
                    <Spinner />
                </Page>
            );
        }
    }
}

ProductPage.propTypes = {}

export default ProductPage;