import React from 'react';
import {Button, Card, Control, Field, Icon, Input, Message, SubTitle, Title} from "vendor/bulma";
import {TopUsersContainer} from "features/stats";
import axios from 'axios';
import Emoji from "../../components/Emoji";
import queryString from "query-string";
import { actions as authActions } from 'ducks/auth';
import { actions as appActions } from 'ducks/app';
import {connect} from "react-redux";
import { validateEmail } from 'lib/utils/random';
import Spinner from "../../components/Spinner";
import ReCaptcha from 'react-google-recaptcha';
import config from 'config.js';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { FaceStack } from "features/users";
import { RegisterPageLayout } from './styled';
import OutboundLink from "components/OutboundLink";

const mapDispatchToProps = (dispatch) => {
    return {
        onConfirmEmail: (token) => {
            dispatch(authActions.login('', '', token));
            dispatch(appActions.toggleNewUser())
        }
    }
}

class AccountActivator extends React.Component {
    state = {
        activating: true,
        failed: false,
    }

    componentDidMount() {
        if (this.props.uid && this.props.token) {
            this.activate()
        }
    }

    activate = async () => {
        try {
            this.setState({activating: true,})
            const response = await axios.get(`/accounts/activate/${this.props.uid}/${this.props.token}/`);
            console.log("Activated", response.data)
            this.props.onConfirmEmail(response.data.token)
        } catch (e) {
            this.setState({failed: true, activating: false,})
        }
    }

    render() {
        return (
            <Card style={{width: "100%"}}>
                <Card.Content>
                    {!this.state.failed && this.state.activating && <center><Spinner text={"Activating your account..."} /></center>}
                    {this.state.failed &&
                        <Message danger>
                            <Message.Body>
                                Could not activate your account. Message us for more information.
                            </Message.Body>
                        </Message>
                    }
                </Card.Content>
            </Card>
        )
    }
}

AccountActivator = connect(
    null,
    mapDispatchToProps
)(AccountActivator);

class RegisterForm extends React.Component {
    state = {
        registered: false,
        email: '',
        emailPrefilled: false,
        code: '',
        username: '',
        password: '',
        showPassword: false,
        repeat_password: '',
        recaptchaToken: '',
        showAdvanced: false,
        isRegistering: false,
        errorMessages: null,
        joinSlack: true,
    }

    componentDidMount() {
        this.captcha.execute()
    }

    arePasswordsEqual = () => {
        return this.state.password === this.state.repeat_password
    }

    toggleShowPassword = () => {
        this.setState({
            showPassword: !this.state.showPassword
        })
    }

    isFormValid = () => {
        if (!this.arePasswordsEqual()) {
            return false
        }

        return true
    }

    submitForm = async () => {
        this.setState({ isRegistering: true })
        const data = {
            email: this.state.email,
            invite_code: this.state.code,
            username: this.state.username,
            password: this.state.password,
            repeat_password: this.state.repeat_password,
            join_slack: this.state.joinSlack,
            recaptcha_token: this.state.recaptchaToken,
        }

        try {
            // also try logging user in right away
            const response = await axios.post('/accounts/register/', data)
            if (response.data.success) {
                this.setState({
                    registered: true
                })
            }
        } catch (e) {
            if (e.response && e.response.data && e.response.status === 400) {
                this.setState({
                    registered: false,
                    isRegistering: false,
                    errorMessages: e.response.data
                })
            }
        }
    }

    verifyCallback = (recaptchaToken) => {
        // Here you will get the final recaptchaToken!!!
        this.setState({
            recaptchaToken: recaptchaToken
        })
    }

    // auto check available username

    renderForm = () => (
        <div>
            {this.state.errorMessages !== null && this.state.errorMessages.non_field_errors !== null &&
                <Message danger>
                    <Message.Body>
                        {this.state.errorMessages.non_field_errors}
                    </Message.Body>
                </Message>
            }
            <Field>
                <label className="label">Username</label>
                <Control className="control has-icons-left">
                    <Icon medium className={"is-left"}>
                        @
                    </Icon>
                    <Input
                        value={this.state.username}
                        onChange={(e) => this.setState({ username: e.target.value.toLowerCase() })}
                        danger={this.state.errorMessages && this.state.errorMessages.username}
                        placeholder="Username" />
                </Control>
                <p className="help">
                    {this.state.errorMessages && this.state.errorMessages.username ?
                        this.state.errorMessages.username
                        :
                        'Pick a username. Be creative!'
                    }
                </p>
            </Field>
            {!this.state.emailPrefilled &&
                <Field>
                    <label className="label">Email address</label>
                    <Control>
                        <Input
                            danger={this.state.errorMessages && this.state.errorMessages.email}
                            value={this.state.email}
                            onChange={(e) => this.setState({ email: e.target.value })}
                            placeholder="Email"
                        />
                    </Control>
                    {this.state.errorMessages && this.state.errorMessages.email ?
                        <p className="help">{this.state.errorMessages.email}</p>
                        :
                        <p className="help">I won't spam you, I promise!</p>
                    }
                </Field>
            }
            <Field>
                <label className="label">Password</label>
                <Control>
                    <Input
                        value={this.state.password}
                        onChange={(e) => this.setState({ password: e.target.value, repeat_password: e.target.value })}
                        type={this.state.showPassword ? null : 'password'}
                        danger={this.state.errorMessages && this.state.errorMessages.password}
                        placeholder="Password" />
                    <p className="help">
                        {
                            // eslint-disable-next-line
                        } <a onClick={this.toggleShowPassword}>{this.state.showPassword ? 'Hide password': 'Show password'}</a>
                    </p>
                </Control>
                {this.state.errorMessages && this.state.errorMessages.password ?
                    <p className="help">{this.state.errorMessages.password}</p>
                    :
                    null
                }
            </Field>

            <Field>
                <label className="checkbox">
                    <input
                        onChange={(e) => this.setState({ joinSlack: e.target.checked })}
                        checked={this.state.joinSlack}
                        type="checkbox" />
                        &nbsp;Join the Slack chat
                </label>
                <p className="help">
                    Interact with other makers in our chat community!  <Emoji emoji={"💬"} />
                </p>
            </Field>

            <hr />

            <center>
                <Button
                    disabled={!this.isFormValid()}
                    medium primary
                    loading={this.state.isRegistering}
                    onClick={this.submitForm}>
                    Create account
                </Button>
            </center>
        </div>
    )

    renderTweetButton = () => {
        const text = `I just joined @getmakerlog! #TogetherWeMake`;
        const url = `${config.BASE_URL}/@${this.state.username}`;

        return (
            <OutboundLink href={`https://twitter.com/share?text=${encodeURIComponent(text)}&url=${url}`}
               className="button is-info" style={{backgroundColor: "#1b95e0"}} target="_blank">
                <Icon><FontAwesomeIcon icon={['fab', 'twitter']} /></Icon> Tweet
            </OutboundLink>
        )
    }

    renderFinishedStep = () => (
        <center>
            <SubTitle>The activation email is on the way! <Emoji emoji={"💌"} /></SubTitle>
            <p className={"help"}>It was sent to {this.state.email}. <br /> If it takes too long to arrive, don't hesitate to message me through the chat button.</p>
            <br />
            {this.renderTweetButton()}
        </center>
    )

    render() {
        return (
            <Card style={{width: "100%"}}>
                <Card.Content>
                    {!this.state.registered && this.renderForm()}
                    {this.state.registered && this.renderFinishedStep()}

                    <ReCaptcha
                        ref={(el) => { this.captcha = el; }}
                        size={"invisible"}
                        sitekey="6LfiJoQUAAAAAD-OhK2h2iy8cgnx3Qk5s9kqeLmb"
                        onChange={this.verifyCallback}
                    />
                </Card.Content>
            </Card>
        )
    }
}

class RegisterPage extends React.Component {
	state = {
        confirming: false,
	    inviteRequested: false,
	    isSubmitting: false,
        bio: '',
        email: '',
        errorMessages: null
	}

	componentDidMount() {
        this.injectCss();
        let params = queryString.parse(this.props.location.search)
        this.params = params;
        if (params.uid && params.token) {
            this.setState({ confirming: true })
        }
    }

    injectCss = () => {
        const nav = document.getElementById('main-navbar');
        nav.classList.add("transparent-navbar");
    }

    removeCss = () => {
        const nav = document.getElementById('main-navbar');
        nav.classList.remove("transparent-navbar");
    }

	isValidEmail = () => {
	    if (this.state.email.length === 0) {
	        return false;
        }

        if (!validateEmail(this.state.email)) {
	        return false;
        }

        return true;
    }

    isValidBio = () => {
	    // not obligatory for now.
	    return true;
    }

    submitRequest = async () => {
	    const data = {
	        'email': this.state.email,
            'bio': this.state.bio,
        }

        try {
	        this.setState({isSubmitting: true})
            await axios.post('/invites/request/', data);
            this.setState({inviteRequested: true})
        } catch (e) {
            if (e.response && e.response.data && e.response.status === 400) {
                this.setState({
                    isSubmitting: false,
                    errorMessages: e.response.data
                })
            }
        }
    }

	render() {
		return (
			<RegisterPageLayout className={"quote-page"} contained={false}>
                <div className="columns is-fullheight accounts-hero">
                    <div className="column tint animated fadeIn" style={{maxWidth: "100%"}}>
                        <Title>
                            Create with us.
                        </Title>
                        <p style={{marginBottom: 20}} className={"is-hidden-mobile"}>
                            <TopUsersContainer component={({ topUsers }) => (
                                <FaceStack is={48} limit={6} users={topUsers} />
                            )} />
                        </p>

                        <div style={{minWidth: 400}}>
                            { this.state.confirming ? <AccountActivator uid={this.params.uid} token={this.params.token}  /> : <RegisterForm  />}
                        </div>
                    </div>
                </div>
			</RegisterPageLayout>
		)
	}
}

export default RegisterPage;