import React from 'react';
import {connect} from "react-redux";
import { actions as streamActions } from 'ducks/stream';
import NoActivityCard from "../../features/stream/components/NoActivityCard/NoActivityCard";
import {FollowingStream} from "features/stream";
import Page from "layouts/Page";
import StreamHeader from "./components/StreamHeader";
import Sidebar from "./components/Sidebar";

class StreamPage extends React.Component {
    isNewUser = () => (
        (this.props.data.length === 0
            && this.props.initialLoaded
            && !this.props.isSyncing)
        || this.props.isNewUser
    )

    render() {


        return (
            <Page contained={false} footer={false}>
                <StreamHeader />

                <div className="container">
                    <div className="columns">
                        <div className="column">
                            {this.isNewUser() ? <NoActivityCard/> : <FollowingStream />}
                        </div>
                        <div className="column sidebar" style={{marginTop: 54}}>
                            <Sidebar />
                        </div>
                    </div>
                </div>
            </Page>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        isNewUser: state.app.isNewUser,
        isSyncing: state.stream.isSyncing,
        errorMessages: state.stream.errorMessages,
        data: state.stream.data,
        allLoaded: state.stream.allLoaded,
        fetchFailed: state.stream.fetchFailed,
        initialLoaded: state.stream.initialLoaded,
        hasMore: state.stream.nextUrl // rely on the nextUrl being truthy
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        init: () => dispatch(streamActions.init()),
        loadMore: () => dispatch(streamActions.loadMore())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StreamPage);