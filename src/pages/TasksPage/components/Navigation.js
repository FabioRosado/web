import React from 'react';
import {Button, Icon} from 'vendor/bulma';
import  { NavLink } from 'react-router-dom';
import SearchBar from "./SearchBar";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { Container } from 'vendor/bulma';
import {connect} from "react-redux";
import {actions as editorActions} from "../../../ducks/editor";

class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
        }
    }

    onClickCloseExpand = () => {
        if (this.state.expanded) {
            this.setState({ expanded: !this.state.expanded })
        }
    }

    render() {
        return (
            <nav className="navbar navbar-secondary is-white Navigation" aria-label="main navigation">
               <Container>
                <div className="navbar-brand is-hidden-desktop">
                        <h1 className="navbar-item">
                            <span className={"title is-5"}>
                                {this.props.location.pathname === '/tasks' ? 'Tasks' : null }
                                {this.props.location.pathname === '/tasks/stats' ? 'Stats' : null }
                                {this.props.location.pathname === '/tasks/products' ? 'Products' : null }
                                {this.props.location.pathname === '/tasks/apps' ? 'Apps' : null }
                            </span>
                        </h1>

                        <div className="navbar-burger" onClick={e => this.setState({ expanded: !this.state.expanded })}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>

                    <div className={this.state.expanded ? "navbar-menu is-active" : "navbar-menu"}>
                        <div className="navbar-start" onClick={this.onClickCloseExpand}>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/tasks" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={'check'} />
                                    </Icon> <span>Today</span>
                            </NavLink>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/tasks/list" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={'tasks'} />
                                </Icon> <span>List</span>
                            </NavLink>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/tasks/kanban" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={'columns'} />
                                </Icon> <span>Kanban</span>
                            </NavLink>
                        </div>
            
                        <div className="navbar-end">
                            <div className="navbar-item">
                                <Button primary onClick={this.props.toggleEditor}>
                                    <Icon medium>
                                        <FontAwesomeIcon icon={'plus'} />
                                    </Icon> Add task
                                </Button>
                            </div>
                            <div className="navbar-item">
                                <SearchBar/>
                            </div>
                        </div>
                    </div>
               </Container>
            </nav>
        )
    }
}


const mapDispatchToProps = (dispatch) => ({
    toggleEditor: () => dispatch(editorActions.toggleEditor())
})

Navigation.propTypes = {}

export default connect(
    null,
    mapDispatchToProps
)(Navigation);