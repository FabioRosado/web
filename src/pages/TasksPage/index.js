import React from 'react';
import {Route, Switch, withRouter} from "react-router-dom";
import Navigation from './components/Navigation';
import {connect} from "react-redux";
import { actions as tasksActions} from 'ducks/tasks';
import Page from "layouts/Page";
import {applySearchTerms} from "lib/utils/tasks";
import {TodayView, KanbanView, ListView} from "features/projects";
import {Container} from "vendor/bulma";


class TasksPage extends React.Component {
    componentDidMount() {
        this.props.connect()
    }

    componentWillUnmount() {
        this.props.disconnect()
    }

    renderCurrentRoute = () => (
        <Switch>
            <Route exact path='/tasks' component={() => (
                <TodayView tasks={this.props.tasks} />
            )} />
            <Route exact path='/tasks/list' component={() => (
                <Container>
                    <ListView projects={this.props.projects} tasks={this.props.tasks} />
                </Container>
            )} />
            <Route exact path='/tasks/kanban' component={() => (
                <Container>
                    <KanbanView tasks={this.props.tasks} />
                </Container>
            )} />
        </Switch>
    )

    render() {
        return (
            <Page contained={false} footer={false} loading={!this.props.ready && !this.props.failed}>
                <Route component={Navigation} />
                {this.renderCurrentRoute()}
            </Page>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        connect: () => dispatch(tasksActions.connect()),
        disconnect: () => dispatch(tasksActions.disconnect()),
        loadTasks: () => dispatch(tasksActions.loadTasks()),
    }
}

const mapStateToProps = (state) => ({
    tasks: applySearchTerms(state.tasks.tasks, state.tasks.searchTerms),
    projects: state.projects.projects,
    isSyncing: state.tasks.isSyncing || state.projects.isSyncing,
    isSilentlySyncing: state.tasks.ready && state.tasks.isSyncing,
    failed: state.tasks.failed,
    errorMessages: state.tasks.errorMessages,
    ready: state.tasks.ready && state.projects.ready,
    searchTerms: state.tasks.searchTerms,
})

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(TasksPage)
);