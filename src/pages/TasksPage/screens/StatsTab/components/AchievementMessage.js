import React from 'react';
import PropTypes from 'prop-types';
import {Icon, Message} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

const AchievementMessage = (props) => (
    <Message success>
        <Message.Header>
            <strong>
                {props.title ? props.title : "You're doing really well!"}
            </strong>
            <Icon>
                <FontAwesomeIcon icon={"trophy"} />
            </Icon>
        </Message.Header>
        <Message.Body>
            {props.children}
        </Message.Body>
    </Message>
)

AchievementMessage.propTypes = {
    title: PropTypes.string
}

export default AchievementMessage;