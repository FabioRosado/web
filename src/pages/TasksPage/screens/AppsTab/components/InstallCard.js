import React from 'react';
import {Hero, SubTitle, Box} from "vendor/bulma";

const InstallCard = (props) => (
    <Box>
        <Hero className={"has-text-centered"}>
            <Hero.Body>
                <SubTitle is='3'>
                    <strong className="has-text-grey">
                        {props.header ? props.header : <span>{props.app} is not installed yet.</span>}
                    </strong>
                    <SubTitle is='5'>
                        <strong className="has-text-grey-light">Click the button below to begin.</strong>
                    </SubTitle>
                    <div>
                        {props.children}
                    </div>
                </SubTitle>
            </Hero.Body>
        </Hero>
    </Box>
)

export default InstallCard;
