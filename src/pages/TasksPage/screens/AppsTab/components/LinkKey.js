import React from 'react';
import { mapStateToProps } from 'ducks/apps';
import { connect } from 'react-redux';

const LinkKey = (props) => (
    <div className="tags has-addons">
        <span className="tag is-dark is-medium">Link key</span>
        <span className="tag is-medium">{props.linkKey}</span>
    </div>
);

export default connect(
    mapStateToProps
)(LinkKey);