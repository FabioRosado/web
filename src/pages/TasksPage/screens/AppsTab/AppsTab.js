import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router';

import NothingHere from './components/NothingHere';
import Spinner from 'components/Spinner';
import Shop from './screens/Shop';
import Slack from './apps/Slack';
import './AppsTab.css';
import Trello from './apps/Trello/Trello';
import GitHub from './apps/GitHub/GitHub';
import Webhooks from './apps/Webhooks/Webhooks';
import NodeHost from './apps/NodeHost/NodeHost';
import Todoist from './apps/Todoist';
import GitLab from "./apps/GitLab/";

class AppsTab extends React.Component {
    componentDidMount() {
        this.props.fetchApps();
    }

    render() {
        if (!this.props.ready) {
            return <Spinner />
        }

        return (
            <div className="AppsTab">
                <Route exact path='/tasks/apps' component={this.props.installedCount === 0 ? NothingHere : Shop} />
                <Route exact path='/tasks/apps/shop' component={Shop} />
                <Route exact path='/tasks/apps/slack' component={Slack} />
                <Route exact path='/tasks/apps/trello' component={Trello} />
                <Route exact path='/tasks/apps/github' component={GitHub} />
                <Route exact path='/tasks/apps/gitlab' component={GitLab} />
                <Route exact path='/tasks/apps/webhook' component={Webhooks} />
                <Route exact path='/tasks/apps/nodehost' component={NodeHost} />
                <Route exact path='/tasks/apps/todoist' component={Todoist} />
            </div>
        )
    }
}



export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppsTab);