import React from 'react';
import { actions as userActions } from 'ducks/user';
import { connect } from 'react-redux';
import {
    Button, Card, Control, Field, Input, Message, SubTitle,
    Title,
    Content, Checkbox
} from "vendor/bulma";
import * as axios from "axios";
import {changePassword, downloadExportedData} from "../../lib/user";
import {requestReport} from "../../lib/management";
import {Experiment} from "../../lib/utils/experiment";
import Embed from 'components/Embed';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Page from 'layouts/Page';
import Sticky from 'react-stickynode';
import ProfileTab from "./tabs/ProfileTab";
import GoldTab from "./tabs/GoldTab";
import SidebarLink from "../../components/SidebarLink";

class SecuritySettings extends React.Component {
    state = {
        isChanging: false,
        failed: false,
        success: false,
        errorMessage: null,
        oldPassword: '',
        newPassword: '',
    }

    onSubmit = async () => {
        this.setState({
            isChanging: true,
            failed: false,
        })
        try {
            const success = await changePassword(
                this.state.oldPassword,
                this.state.newPassword
            )

            if (success) {
                this.setState({
                    success: true,
                    oldPassword: '',
                    newPassword: '',
                })
            } else {
                throw new Error("Password change failed, and server didn't return why.")
            }
        } catch (e) {
            this.setState({
                failed: true
            })
        }
    }

    renderErrors = () => {
        if (this.state.failed) {
            return (
                <Message danger>
                    <Message.Body>
                        Your old password seems incorrect. Please try again.
                    </Message.Body>
                </Message>
            )
        } else {
            return null;
        }
    }

    renderSuccess = () => {
        if (this.state.success) {
            return (
                <Message success>
                    <Message.Body>
                        Password changed.
                    </Message.Body>
                </Message>
            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <div>
                <SubTitle is={"4"}>
                    Change your password
                </SubTitle>
                {this.renderSuccess()}
                {this.renderErrors()}
                <label className="label">Old password</label>
                <Field hasAddons>
                    <Control>
                        <Input
                            type="password"
                            onChange={(e) => this.setState({ oldPassword: e.target.value })} />
                    </Control>
                </Field>
                <label className="label">New password</label>
                <Field hasAddons>
                    <Control>
                        <Input
                            type="password"
                            onChange={(e) => this.setState({ newPassword: e.target.value })} />
                    </Control>
                </Field>
                <Button onClick={this.onSubmit}>Change password</Button>
            </div>
        )
    }
}

class DataSettings extends React.Component {
    state = {
        errored: false
    }

    onSubmit = async () => {
        try {
            await downloadExportedData()
        } catch (e) {
            this.setState({ errored: true })
        }
    }

    renderErrors = () => {
        if (this.state.errored) {
            return (
                <Message danger>
                    <Message.Body>
                        Couldn't export data. Too many export requests for today, try again tomorrow.
                    </Message.Body>
                </Message>
            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <div>
                <SubTitle is={"4"}>
                    <strong>I strongly believe in data ownership. Your data is yours, no matter what.</strong> <br />
                </SubTitle>
                <hr />
                <Content>
                    <SubTitle is={"4"}>
                        Data usage policies
                    </SubTitle>
                    <p>
                        <ol>
                            <li>I treat your personal data with utmost respect and attention.</li>
                            <li>Makerlog erases your data once you hit the delete button. No hidden bullcrap. It's always deleted no matter what.</li>
                            <li>You will be notified about breaches as soon as possible, always within the GPDR timeframe (but we will strive to do sooner).</li>
                            <li>In terms of PII, we store your e-mail, full name, hashed password (PBKDF2).</li>
                        </ol>
                    </p>
                </Content>
                <hr />
                <SubTitle is={"4"}>
                    Download your data
                </SubTitle>
                <p>
                    Click the button below to request a copy of all your data on Makerlog. <br />
                    It'll be serialized in super-simple, easy to read JSON. <br />
                    No obfuscation. No bullshit.
                    <br />

                    <em>
                        <small>
                            Note: To prevent server load spikes, Makerlog allows you to export your data maximum 3 times a day.
                        </small>
                    </em>
                </p>
                <br />
                {this.renderErrors()}
                <Button medium primary onClick={this.onSubmit}>
                    Download your data
                </Button>
            </div>
        )
    }
}

class Broadcaster extends React.Component {
    state = {
        isLoading: false,
        message: '',
        link: '',
        failed: false,
    }

    broadcast = async () => {
        try {
            this.setState({ isLoading: true })
            let result = await axios.post('/notifications/management/broadcast/', { content: this.state.message, broadcast_link: this.state.link });
            this.setState({
                isLoading: false,
                failed: !result.data.success
            })
        } catch (e) {

        }
    }

    render() {
        return (
            <div>
                {this.state.failed && <Message danger><Message.Body>Failed to broadcast.</Message.Body></Message>}
                <Input placeholder="Message" value={this.state.message} onChange={(e) => this.setState({ message: e.target.value})} />
                <Input placeholder="Link" value={this.state.link} onChange={(e) => this.setState({ link: e.target.value})} />
                <Button onClick={this.broadcast} loading={this.state.isLoading}>Broadcast</Button>
            </div>
        )
    }
}

class ManagementPanel extends React.Component {
    state = {
        requestingReport: false,
        failed: false,
    }

    requestReport = async () => {
        try {
            this.setState({ requestingReport: true })
            await requestReport()
            this.setState({ requestingReport: false })
        } catch (e) {
            this.setState({
                requestingReport: false,
                failed: true,
            })
        }
    }

    render() {
        return (
            <div>
                <Message info>
                    <Message.Body>
                        <strong>Welcome to the Makerlog staff management panel.</strong> If you aren't staff, you shouldn't be here. Either way, nothing will work (API controls mwahaha). Go away.
                    </Message.Body>
                </Message>
                {this.state.failed &&
                    <Message danger>
                        <Message.Body>
                            Something went wrong, or you don't have permission to do this.
                        </Message.Body>
                    </Message>
                }
                <hr />
                <SubTitle is={'3'}>Discord</SubTitle>
                <Button onClick={this.requestReport} loading={this.state.requestingReport}>Send daily report</Button>
                <hr />
                <SubTitle is={'3'}>Broadcaster</SubTitle>
                <Broadcaster />
            </div>
        )
    }
}

class ExperimentalSettings extends React.Component {
    constructor(props) {
        super(props);

        this.experiment = new Experiment("headerAsBackground");

        this.state = {
            headerAsBackground: this.experiment.isEnabled()
        }
    }

    headerAsBackground = (bool) => {
        this.setState({ headerAsBackground: bool })
        this.experiment.setStatus(bool)
    }

    render() {
        return (
            <div>
                <Title is={"3"}>
                    Labs
                </Title>
                <Content>
                    Here's a few experimental features we're testing.
                </Content>
                <hr />
                <Field>
                    <Checkbox checked={this.state.headerAsBackground} onChange={e => this.headerAsBackground(e.target.checked)}>
                        Header image as background
                    </Checkbox>
                </Field>
            </div>
        )
    }
}

class EmbedSettings extends React.Component {
    render() {
        return (
            <div>
                <Content>
                    Here's a few embeds you can plug anywhere to add a little Makerlog spice, and showcase your stats.
                </Content>
                <div className="columns">
                    <div className="column">
                        <SubTitle is="4">
                            Stats embed:
                        </SubTitle>
                        <div style={{maxWidth: 500}}>
                            <Embed stats url={`/users/${this.props.user.id}/stats_embed`} />
                        </div>
                    </div>
                    <div className="column">
                        <SubTitle is="4">
                            Done this week:
                        </SubTitle>
                        <div style={{maxWidth: 400}}>
                            <Embed user url={`/users/${this.props.user.id}/embed`} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const embedStateToProps = (state) => {
	return {
		user: state.user.me,
	}
}

EmbedSettings = connect(
	embedStateToProps
)(EmbedSettings)

class SettingsPage extends React.Component {
    state = {
        activeTab: 1,
    }

    componentDidMount() {
        // update user info.
        this.props.fetchUser();
    }

    switchToTab = (index) => {
        this.setState({
            activeTab: index
        })
    }

    renderTabLink = (name, index, icon=null) => (
        <SidebarLink onClick={() => this.switchToTab(index)} active={this.state.activeTab === index}>
            <span className={"menu-icon"}><FontAwesomeIcon icon={icon} /></span>
            <span>{name}</span>
        </SidebarLink>
    )

    render() {
        return (
            <Page className="SettingsPage">
                <div className={"columns"}>
                    <div className={"column is-one-fifth"}>
                        <Sticky enabled={window.innerWidth >= 728} top={30}>
                            <Card>
                                <Card.Content>
                                    {this.renderTabLink("You", 1, 'user-circle')}
                                    {this.renderTabLink("Gold", 7, "check-circle")}
                                    {this.renderTabLink("Embeds", 6, 'code')}
                                    {this.renderTabLink("Security", 2, 'lock')}
                                    {this.renderTabLink("Data", 3, 'tasks')}
                                </Card.Content>
                            </Card>
                        </Sticky>
                    </div>
                    <div className={"column"}>
                        <Card>
                            <Card.Content>
                                {this.state.activeTab === 1 && <ProfileTab updateUser={this.props.updateUser} />}
                                {this.state.activeTab === 2 && <SecuritySettings />}
                                {this.state.activeTab === 3 && <DataSettings />}
                                {this.state.activeTab === 4 && <ManagementPanel />}
                                {this.state.activeTab === 5 && <ExperimentalSettings />}
                                {this.state.activeTab === 6 && <EmbedSettings />}
                                {this.state.activeTab === 7 && <GoldTab user={this.props.user} updateUser={this.props.updateUser} />}
                            </Card.Content>
                        </Card>
                    </div>
                </div>
            </Page>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchUser: () => dispatch(userActions.loadUser()),
    updateUser: (user) => dispatch(userActions.updateUser(user))
})

const mapStateToProps = (state) => ({
    isLoading: state.user.isLoading,
    user: state.user.me,
})

SettingsPage.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsPage);