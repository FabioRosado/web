import React from 'react';
import {
   Control,
    Field,
} from "vendor/bulma";
import {me, patchSettings} from "lib/user";
import {ChromePicker} from "react-color";
import Emoji from "../../../components/Emoji";
import Switch from 'react-switch';
import Colors from 'styles/colors';
import {Button, Icon, Title} from "../../../vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import GoldIcon from "../../../GoldIcon";
import OutboundLink from "../../../components/OutboundLink";

class GoldTab extends React.Component {
    state = {
        loading: true,
        isPosting: false,
        accent: '',
        dark_mode: false,
        failed: false,
    }

    componentDidMount() {
        this.prefillFields()
    }

    onSubmit = async (event) => {
        if (event.preventDefault) {
            event.preventDefault();
        }
        this.setState({ loading: true })
        try {
            const user = await patchSettings({
                accent: this.state.accent,
                dark_mode: this.state.dark_mode,
            })
            if (this.props.updateUser) {
                this.props.updateUser(user)
            }
            this.setState({ loading: false })
        } catch (e) {

        }
    }


    prefillFields = async () => {
        try {
            const data = await me();
            this.setState({
                loading: false,
                accent: data.accent,
                dark_mode: data.dark_mode,
            })
        } catch (e) {

        }
    }

    onChangeColor = async (color, event) => {
        await this.setState({
            accent: color.hex,
        })
        await this.onSubmit(event)
    }

    renderErrorMessages = () => {
        let messages = [];
        let errors = this.state.errorMessages;
        if (typeof errors === 'object') {
            for (let key in errors) {
                messages.push(
                    <p>
                        <strong>{key.replace(/[_-]/g, " ")}</strong>: {errors[key]}
                    </p>
                )
            }

            return messages
        } else if (errors.constructor === Array) {
            errors.map((err) => {
                messages.push(
                    <p>{err}</p>
                )

                return true;
            })
        } else {
            return <p>{errors}</p>;
        }

        return messages
    }

    onChangeDarkMode = async (e) => {
        await this.setState({
            dark_mode: !this.state.dark_mode
        })

        await this.onSubmit(e)
    }

    resetDefaultColor = async () => {
        await this.setState({
            accent: new Colors().primary,
        })

        await this.onSubmit({})
    }

    render() {
        if (!this.props.user.gold) {
            return (
                <div>
                    <Title is={4}>You don't have Makerlog Gold yet. <GoldIcon /></Title>
                    <p>
                        <strong>Makerlog Gold is the premium plan for Makerlog</strong>, with value added features like Dark Mode, accent color changing, NO ADS, and more.
                        <br /><strong>It's just $5/mo</strong> and you'll help me keep making Makerlog for years to come.
                    </p>
                    <br />
                    <div>
                        <OutboundLink className={"button is-primary is-medium is-rounded"} to={"https://pay.paddle.com/checkout/547895"} target={'_blank'}>
                            <Icon><FontAwesomeIcon icon={'check-circle'} /></Icon> Get Gold now
                        </OutboundLink>
                    </div>
                </div>
            )
        }

        return (
            <div>
                <label className="label">Dark mode</label>
                <Field hasAddons>
                    <Control>
                        <Switch
                            checkedIcon={
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        height: "100%",
                                        fontSize: 15,
                                        paddingRight: 2
                                    }}
                                >
                                    <Emoji emoji="🌑" />
                                </div>}
                            uncheckedIcon={
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        height: "100%",
                                        fontSize: 15,
                                        paddingRight: 2
                                    }}
                                >
                                    <Emoji emoji="☀️" />
                                </div>}
                            onColor="#47E0A0"
                            height={30}
                            width={60}
                            handleDiameter={20}
                            checked={this.state.dark_mode}
                            onChange={this.onChangeDarkMode}
                        />
                    </Control>
                </Field>
                <label className="label">Accent color</label>
                <Field hasAddons>
                    <Control>
                        <ChromePicker disableAlpha color={this.state.accent} onChangeComplete={this.onChangeColor} />
                    </Control>
                </Field>
                <Field>
                    <Control>
                        <Button small text onClick={this.resetDefaultColor}>Reset to default</Button>
                    </Control>
                </Field>
            </div>
        )
    }
}

export default GoldTab;