import React from 'react';
import Page from 'layouts/Page';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Hero, Container, Title, SubTitle, Button } from 'vendor/bulma';
import { GlobalStream } from 'features/stream';
import Sidebar from './Sidebar';

const HomeHero = styled(Hero)`
    color: ${props => props.theme.primaryDarker} !important;
    
    .hero-body {
        position: relative;
    }
`

const ThumbnailGrid = styled.div`
    display: flex; 
    position: absolute;
    right: 0px;
    width: 100%;
    flex-wrap: wrap;
    top: 0px;
    height: 100%;
    z-index: 4;
    background-image: url(https://i.imgur.com/llKaQpR.jpg);
    background-position: center;
    overflow: hidden;
    
    .image {
        flex: 1;
        min-height: 40px;
        min-width: 40px;
        max-height: 40px;
        max-width: 40px;
        height: auto;
        overflow: hidden;
    }
    .image > img {
        min-height: 40px;
        min-width: 40px;      
        height: auto;
        
    }
`

const ThumbnailCover = styled.div`
    display: flex; 
    position: absolute;
    right: 0px;
    width: 100%;
    flex-wrap: wrap;
    top: 0px;
    overflow: hidden;
    height: 100%;
    z-index: 6;
    background-color: rgba(83,175,135, 0.9)
`

export default () => (
    <Page footer={false} contained={false}>
        <HomeHero primary bold>
            <HomeHero.Body>
                <ThumbnailCover />
                <ThumbnailGrid />

                <Container style={{zIndex: 10}}>
                    <Title is={"3"}>
                        Get things done with us.
                    </Title>
                    <SubTitle>
                        Makerlog is a community of 2,000+ makers building products together.
                    </SubTitle>
                    <Link to='/begin'><Button white className="is-rounded">Get started</Button></Link>
                </Container>
            </HomeHero.Body>
        </HomeHero>
        <Container>
            <div className="columns" style={{marginTop: 20}}>
                <div className="column">
                    <GlobalStream />
                </div>
                <div className="column sidebar" style={{marginTop: 54}}>
                    <Sidebar />
                </div>
            </div>
        </Container>
    </Page>
)