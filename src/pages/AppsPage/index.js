import React from 'react';
import Page from 'layouts/Page';
import {Hero as BaseHero, Title, Media, Heading, SubTitle, Level, Card as BulmaCard} from 'vendor/bulma';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {Link, Route, Switch} from 'react-router-dom';
import styled from 'styled-components';
import {connect} from 'react-redux';
import {mapStateToProps, mapDispatchToProps} from "ducks/apps";
import Emoji from "components/Emoji";
import AppList from "./components/AppList";
import {userIsAuthenticated} from "../../layouts/App/authRules";
import GitHub from "./apps/GitHub";
import Slack from "./apps/Slack";
import Trello from "./apps/Trello";
import GitLab from "./apps/GitLab";
import Webhooks from "./apps/Webhooks";
import NodeHost from "./apps/NodeHost";
import Todoist from "./apps/Todoist";
import Shipstreams from "./apps/Shipstreams";
import Tele from "./apps/Telegram";
import withCurrentUser from "../../features/users/containers/withCurrentUser";

const PageHero = styled(BaseHero)`
    background-color: ${props => props.theme.primaryDarker} !important;
    margin-bottom: 30px;

    background: linear-gradient(rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.15)), linear-gradient(221deg, rgba(71, 224, 160, 0.9), rgba(0, 167, 97, 0.9), rgba(0, 151, 167, 0.9), rgba(0, 99, 167, 0.9), rgba(92, 0, 167, 0.9));
  background-size: 1000% 1000%;
  -webkit-animation: makerlog-2018 30s ease infinite;
  animation: makerlog-2018 30s ease infinite;

  @-webkit-keyframes makerlog-2018 {
  0%{background-position:82% 0%}
  50%{background-position:19% 100%}
  100%{background-position:82% 0%}
}
@-moz-keyframes makerlog-2018 {
  0%{background-position:82% 0%}
  50%{background-position:19% 100%}
  100%{background-position:82% 0%}
}
@keyframes makerlog-2018 {
  0%{background-position:82% 0%}
  50%{background-position:19% 100%}
  100%{background-position:82% 0%}
}

`

const AppHero = styled(BaseHero)`
    position: relative;
    border-radius: 10px;
`

let BaseCard = styled(BulmaCard)`
  & .title:not(.is-spaced) + .subtitle  {
    margin-top: -1.2rem !important;
  }
`

const MenubarCard = styled(BaseCard)`
    background-image: url(https://cnet2.cbsistatic.com/img/-qvJVYfnJOTphXGOc2fzSNnwQPk=/1600x900/2018/06/07/78e06ce4-81e0-4b35-992f-6a2b3585b931/mojave-night.jpg);
    background-size: cover;
    color: white !important;
    height: 100%;
    display: flex;
    align-items: center;
`

const GoogleAssistantCard = styled(BaseCard)`
    display: flex;
    align-items: center;
    
    & .title {
      color: #4885ed !important;
    }
    
    & .subtitle {
      color: #4885ed !important;
    }
`


const MakerlogSearchCard = styled(BaseCard)`
    display: flex;
    align-items: center;
    justify-content: center;
    
    & .card-content {
      width: 100%;
    }
`

const TodayForMakerlogCard = styled(BaseCard)`
    display: flex;
    align-items: center;
    
    & .card-content {
      width: 100%;
    }
    
    & img {
      border-radius: 50%;
    }
    
    & .title, & .subtitle {
     color: #00B77A;
    }
`

const Shop = (props) => {
	return (
		<div>
			<PageHero dark>
				<PageHero.Body>
					<div className="container">
						<center>
							<Title is="3">Apps for Makerlog</Title>
							<SubTitle>
								Integrations, apps, community creations, and more!
							</SubTitle>
						</center>
					</div>
				</PageHero.Body>
			</PageHero>
			<div className="container">
				<div className="columns">
					<div className="column">
						<AppHero primary bold>
							<AppHero.Body>
								<Media>
									<Media.Content>
										<Heading>
											<FontAwesomeIcon icon={['far', 'star']}/> Featured app
										</Heading>
										<Title is="5">
											Logger for Makerlog
										</Title>
										<SubTitle is="6">
											The unofficial Makerlog client for iOS and Android, by
											maker <Link to="/@arnav">Arnav Puri</Link>.
										</SubTitle>
										<Level>
											<Level.Left>
												<Level.Item>
													<a href="https://play.google.com/store/apps/details?id=com.brownfingers.getmakerlog">
														<img alt={"Play Store"} style={{height: 40}} src="/assets/img/play-store.png"/>
													</a>
												</Level.Item>
											</Level.Left>
										</Level>
									</Media.Content>
									<Media.Right>
										<img className="is-hidden-mobile" style={{
											height: 200,
											marginTop: '-10px',
											marginBottom: '-55px'
										}} alt="app screenshot" src="https://i.imgur.com/c66dz6Y.png"/>
									</Media.Right>
								</Media>
							</AppHero.Body>
						</AppHero>
					</div>

					<div className="column is-4">
						<MenubarCard>
							<MenubarCard.Content>
								<Title is="5" className="has-text-white">
									Makerlog Menubar
								</Title>
								<SubTitle is="6" className="has-text-white">
									A super fast menubar app for macOS.
								</SubTitle>
								<a href="https://menubar.getmakerlog.com/" className="button is-rounded">
									Get it now
								</a>
							</MenubarCard.Content>
						</MenubarCard>
					</div>
				</div>

				<div className="tile is-ancestor">
					<div className="tile is-parent">
						<GoogleAssistantCard className="tile is-child">
							<GoogleAssistantCard.Content>
								<Media>
									<Media.Content>
										<Title is="5">
											Makerlog for Google Assistant
										</Title>
										<SubTitle is="6">
											Productivity right from your assistant.
										</SubTitle>
										<a href="https://assistant.getmakerlog.com/" className="button is-rounded">
											Get it now
										</a>
									</Media.Content>
									<Media.Right>
										<img src={"/assets/img/google-assistant.png"} width={80} alt={"Google Assistant"}/>
									</Media.Right>
								</Media>
							</GoogleAssistantCard.Content>
						</GoogleAssistantCard>
					</div>
					<div className="tile is-parent">
						<TodayForMakerlogCard className="tile is-child">
							<TodayForMakerlogCard.Content>
								<Media>
									<Media.Content>
										<Title is="5">
											Today for Makerlog
										</Title>
										<SubTitle is="6">
											A simple task manager for Makerlog.
										</SubTitle>
										<a href="https://today.getmakerlog.com/" className="button is-rounded">
											Get it now
										</a>
									</Media.Content>
									<Media.Right>
										<img src={"https://today.getmakerlog.com/apple-touch-icon.png"} width={80} alt={"Today logo"}/>
									</Media.Right>
								</Media>
							</TodayForMakerlogCard.Content>
						</TodayForMakerlogCard>
					</div>
					<div className="tile is-parent">
						<MakerlogSearchCard className="tile is-child">
							<MakerlogSearchCard.Content>
								<Media>
									<Media.Content>
										<Title is="5">
											Makerlog Search
										</Title>
										<SubTitle is="6">
											Search all products on Makerlog!
										</SubTitle>
										<a href="https://makerlog-search.netlify.com/" className="button is-rounded">
											Get it now
										</a>
									</Media.Content>
									<Media.Right>
										<Title><Emoji emoji={"🔎"}/></Title>
									</Media.Right>
								</Media>
							</MakerlogSearchCard.Content>
						</MakerlogSearchCard>
					</div>
				</div>

				{
					// Begin official integrations
				}

				<hr/>

				<AppList/>
			</div>
		</div>
	);
}

// prevent double renders
// FUCK THIS SHIT I'M PISSED AS FUCK
let Telegram = userIsAuthenticated(Tele)

class AppsPage extends React.Component {
	componentDidMount() {
		this.props.fetchApps();
	}

	render() {

		return (
			<Page contained={false} loading={this.props.isLoading || (this.props.isLoggedIn && !this.props.apps)}>
				<Switch>
					<Route exact path='/apps' component={Shop}/>
					<Route exact path='/apps/slack' component={userIsAuthenticated(Slack)}/>
					<Route exact path='/apps/trello' component={userIsAuthenticated(Trello)}/>
					<Route exact path='/apps/github' component={userIsAuthenticated(GitHub)}/>
					<Route exact path='/apps/gitlab' component={userIsAuthenticated(GitLab)}/>
					<Route exact path='/apps/webhooks' component={userIsAuthenticated(Webhooks)}/>
					<Route exact path='/apps/nodehost' component={userIsAuthenticated(NodeHost)}/>
					<Route exact path='/apps/todoist' component={userIsAuthenticated(Todoist)}/>
					<Route exact path='/apps/shipstreams' component={userIsAuthenticated(Shipstreams)}/>
                    <Route exact path='/apps/telegram' component={Telegram}/>
				</Switch>
			</Page>
		)
	}
}


export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withCurrentUser(AppsPage));