import React from 'react';
import { connect } from 'react-redux';
import {Container, Hero, SubTitle, Title} from "vendor/bulma";
import { errorArray } from 'lib/utils/error';
import { mapStateToProps, mapDispatchToProps } from "ducks/apps";
import Spinner from "components/Spinner";
import InstallCard from '../../../TasksPage/screens/AppsTab/components/InstallCard';

const DiscordInstallCard = (props) => (
    <div className="DiscordInstallCard">
        <InstallCard
            app="Discord">
            Add
        </InstallCard>
    </div>
)

class Discord extends React.Component {
    render() {
        const style = {
            backgroundColor: "#7289da",
            color: 'white'
        }

        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        return (
            <div className="Discord">
                <Hero style={style} dark>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                Discord
                            </Title>
                            <SubTitle>
                                Makerlog's official chat community. Interact with other users, add logs from desktop and mobile, and stay in the loop wherever you go.
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <br />

                {!this.props.apps['discord'].installed && <DiscordInstallCard />}
            </div>
        )
    }
}

Discord.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Discord);