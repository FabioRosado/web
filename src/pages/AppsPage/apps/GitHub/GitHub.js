import React from 'react';
import { connect } from 'react-redux';
import {Container, Hero, SubTitle, Title, Card} from "vendor/bulma";
import { mapStateToProps, mapDispatchToProps } from "ducks/apps";
import Spinner from "components/Spinner";
import { WebhookTable } from '../Webhooks/components/WebhooksTable';
import AppWebhookCreator from '../Webhooks/components/AppWebhookCreator';

class GitHub extends React.Component {
    render() {
        const style = {
            backgroundColor: "black",
            color: 'white',
        }

        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        return (
            <div>
                <Hero style={style} dark>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                GitHub
                            </Title>
                            <SubTitle>
                                Log your commits and closed issues from GitHub instantly.
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <br />
                <div className={"container"}>
                    <SubTitle is='5'>Link to project</SubTitle>
                    <Card>
                        <Card.Content>
                            Select a project then use the secret generated webhook to link it to a GitHub repository. We'll start tracking events like commits and issues afterwards.
                            <br />
                            <small className='has-text-grey'>Need help? <a href="https://developer.github.com/webhooks/creating/">Learn how to use this webhook.</a></small>
                            <hr />
                            <AppWebhookCreator appName="GitHub" identifier="github" />
                        </Card.Content>
                    </Card>
                    <br />
                    <SubTitle is='5'>Active webhooks</SubTitle>
                    <Card>
                        <Card.Content>
                            {this.props.apps['github'].installed &&
                            <WebhookTable webhooks={this.props.apps['github'].webhooks} />
                            }
                            {!this.props.apps['github'].installed &&
                            <center><SubTitle>Nothing here.</SubTitle></center>
                            }
                        </Card.Content>
                    </Card>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GitHub);