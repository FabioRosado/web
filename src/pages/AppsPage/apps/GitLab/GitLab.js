import React from 'react';
import { connect } from 'react-redux';
import {Container, Hero, SubTitle, Title, Card} from "vendor/bulma";
import { mapStateToProps, mapDispatchToProps } from "ducks/apps";
import Spinner from "components/Spinner";
import { WebhookTable } from '../Webhooks/components/WebhooksTable';
import AppWebhookCreator from '../Webhooks/components/AppWebhookCreator';

class GitLab extends React.Component {
    render() {
        const style = {
            backgroundColor: "#fc6d26",
            borderRadius: 5,
            color: 'white',
        }

        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        return (
            <div>
                <Hero style={style} dark>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                GitLab
                            </Title>
                            <SubTitle>
                                Log your commits and closed issues from GitLab instantly.
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <Container>
                    <br />
                    <SubTitle is='5'>Link to project</SubTitle>
                    <Card>
                        <Card.Content>
                            Select a project then use the secret generated webhook to link it to a GitLab repository. We'll start tracking events like commits afterwards.
                            <br />
                            <small className='has-text-grey'>Need help? <a href="https://docs.gitlab.com/ee/user/project/integrations/webhooks.html/">Learn how to use this webhook.</a></small>
                            <hr />
                            <AppWebhookCreator appName="GitLab" identifier="gitlab" />
                        </Card.Content>
                    </Card>
                    <br />
                    <SubTitle is='5'>Active webhooks</SubTitle>
                    <Card>
                        <Card.Content>
                            {this.props.apps['gitlab'] && this.props.apps['gitlab'].installed &&
                                <WebhookTable webhooks={this.props.apps['gitlab'].webhooks} />
                            }
                            {this.props.apps['gitlab'] && !this.props.apps['gitlab'].installed &&
                                <center><SubTitle>Nothing here.</SubTitle></center>
                            }
                        </Card.Content>
                    </Card>
                </Container>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GitLab);