import React from 'react';
import {Button, SubTitle, Box,} from "vendor/bulma";
import Spinner from "components/Spinner";
import { getProjects } from 'lib/user';
import { createWebhook } from 'lib/apps';

class AppWebhookCreator extends React.Component {
    constructor(props) {
        super(props);

        this.initialState = {
            ready: false,
            linked: false,
            projects: [],
            hook: null,
            selectedProject: null,
            redirecting: false,
            failed: false,
        }

        this.state = this.initialState;

        this.onSubmit = this.onSubmit.bind(this);
        this.loadData = this.loadData.bind(this);
    }

    async componentDidMount() {
        await this.loadData();
    }

    async loadData() {
        try {
            const projects = await getProjects();
            const ready = true;
            const failed = false;
            this.setState({ projects, ready, failed });
        } catch (e) {
            this.setState({
                ready: false,
                failed: true,
                projects: [],
                linked: false,
            })
        }
    }

    async onSubmit() {
        try {
            const hook = await createWebhook(this.props.identifier, this.state.selectedProject);
            if (this.props.sendKeyTo) {
                var root = window.location.protocol + '//' + window.location.hostname;
                this.setState({ redirecting: true });
                window.location.replace(`${this.props.sendKeyTo}?key=${hook.token}&url=${root}/tasks/apps/nodehost?success=true`);
            }
            this.setState({ linked: true, hook: hook, failed: false, })
        } catch (e) {
            this.setState({ failed: true })
        }
    }

    render() {
        if (this.state.ready && this.state.projects.length === 0 && this.state.hook === null) {
            return (
                <Box>
                    <center>No projects available. <Button text small onClick={this.onSubmit}>Make a non-linked webhook</Button></center>
                </Box>
            )
        } else if (this.state.failed) {
            return (
                <Box>
                    <center>
                        That didn't work. <button onClick={async () => await this.loadData()}>Try again.</button>
                    </center>
                </Box>
            )
        } else if (this.state.redirecting) {
            return (
                <Box>
                    <SubTitle>Redirecting you to the application. This may take a few moments.</SubTitle>
                </Box>
            )
        } else if (this.state.ready && !this.state.linked) {
            return (
                <Box>
                    <center>
                        <div className="field">
                            <label class="label">Select a project to create a linked webhook:</label>
                        </div>
                        <div className="field">
                            <div className="select is-medium">
                                <select value={this.state.selectedProject} onChange={e => this.setState({ selectedProject: e.target.value })}>
                                        <option>Choose a project...</option>
                                        {this.state.projects.map(
                                            project => <option value={project.id}>#{project.name}</option>
                                        )}
                                </select>
                            </div>
                        </div>

                        <div className="field">
                            <Button disabled={!this.state.selectedProject} link onClick={this.onSubmit}>Create linked webhook</Button>
                        </div>
                        <div className="field">
                            <Button text small onClick={this.onSubmit}>...or make a non-linked webhook</Button>
                        </div>
                    </center>
                </Box>
            )
        } else if (this.state.linked && this.state.ready) {
            return (
                <Box>
                    <center>
                        <div className="field">
                            <label class="label">Here's your webhook for {this.props.appName}. Keep it secret; we won't show it to you again.</label>
                        </div>
                        <div className="field">
                            <pre>{this.state.hook.url}</pre>
                        </div>
                    </center>
                </Box>
            )
        } else {
            return <Spinner />
        }
    }
}

export default AppWebhookCreator;