import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {Container, Hero, SubTitle, Title, Table, Card} from "vendor/bulma";
import { WebhookTable } from './components/WebhooksTable';
import { mapStateToProps, mapDispatchToProps } from "ducks/apps";
import Spinner from "components/Spinner";
import AppWebhookCreator from './components/AppWebhookCreator';

const WebhookDocsTable = (props) => (
    <div className="columns">
        <div className="column">
            <SubTitle is='5'>Parameters</SubTitle>
            <Table className="is-fullwidth">
                <Table.Head>
                    <Table.Tr>
                        <Table.Th>Parameter</Table.Th>
                        <Table.Th>Type</Table.Th>
                        <Table.Th>Required</Table.Th>
                        <Table.Th>Description</Table.Th>
                    </Table.Tr>
                </Table.Head>
                <Table.Body>
                    <Table.Tr>
                        <Table.Td>content</Table.Td>
                        <Table.Td>String</Table.Td>
                        <Table.Td>Yes</Table.Td>
                        <Table.Td>The content of the log to post.</Table.Td>
                    </Table.Tr>
                    <Table.Tr>
                        <Table.Td>done</Table.Td>
                        <Table.Td>Boolean</Table.Td>
                        <Table.Td>Yes</Table.Td>
                        <Table.Td>Is it a done task?</Table.Td>
                    </Table.Tr>
                </Table.Body>
            </Table>
        </div>
        <div className="column">
            <SubTitle is='5'>Return values</SubTitle>
            <Table className="is-fullwidth">
                <Table.Head>
                    <Table.Tr>
                        <Table.Th>Parameter</Table.Th>
                        <Table.Th>Type</Table.Th>
                        <Table.Th>Description</Table.Th>
                    </Table.Tr>
                </Table.Head>
                <Table.Body>
                    <Table.Tr>
                        <Table.Td>content</Table.Td>
                        <Table.Td>String</Table.Td>
                        <Table.Td>The content of the log created.</Table.Td>
                    </Table.Tr>
                    <Table.Tr>
                        <Table.Td>done</Table.Td>
                        <Table.Td>String</Table.Td>
                        <Table.Td>Whether it was marked as done</Table.Td>
                    </Table.Tr>
                    <Table.Tr>
                        <Table.Td>done_at</Table.Td>
                        <Table.Td>Timestamp</Table.Td>
                        <Table.Td>This will be returned depending if done is true.</Table.Td>
                    </Table.Tr>
                </Table.Body>
            </Table>
        </div>
    </div>
)

class Webhooks extends React.Component {
    render() {
        const style = {
            backgroundColor: "#27ae60",
            color: 'black'
        }

        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        return (
            <div className="Trello">
                <Hero style={style} dark>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                Webhooks
                            </Title>
                            <SubTitle>
                                Integrate your apps with Makerlog. Terminal, web, server, anywhere. Just POST and go.
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <Container>
                    <br />

                    <SubTitle is='5'>Create webhook</SubTitle>
                    <Card>
                        <Card.Content>
                            Select a project, and we'll create a webhook linked to it. Afterwards, just send a POST event from anywhere, and we'll log to your feed. It's that simple. No authentication required.
                            <hr />
                            <AppWebhookCreator appName="Makerlog" identifier="webhook" />
                        </Card.Content>
                    </Card>

                    <br />
                    <SubTitle is='5'>Documentation</SubTitle>
                    <Card>
                        <Card.Content>
                            <WebhookDocsTable />
                        </Card.Content>
                    </Card>

                    <br />
                    <SubTitle is='5'>Active webhooks</SubTitle>
                    <Card>
                        <Card.Content>
                            {this.props.apps['webhook'].installed &&
                            <WebhookTable webhooks={this.props.apps['webhook'].webhooks} />
                            }
                            {!this.props.apps['webhook'].installed &&
                            <center><SubTitle>Nothing here.</SubTitle></center>
                            }
                        </Card.Content>
                    </Card>
                </Container>
            </div>
        )
    }
}

Webhooks.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(Webhooks));