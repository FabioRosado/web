import React from 'react';
import { connect } from 'react-redux';
import {Container, Hero, SubTitle, Title} from "vendor/bulma";
import { mapStateToProps, mapDispatchToProps } from "ducks/apps";
import Spinner from "components/Spinner";
import {Card, Message} from "../../../../vendor/bulma";
import OutboundLink from "../../../../components/OutboundLink";
import { link } from 'lib/integrations/telegram';

function getParam(param){
    return new URLSearchParams(window.location.search).get(param);
}

class Telegram extends React.Component {
    state = {
        linking: false,
        success: false,
        failed: false,
    }

    componentDidMount() {
        console.log("Mounted")
        if (getParam('key')) {
            this.link(getParam('key'))
        }
    }

    link = async (key) => {
        this.setState({
            linking: true,
            success: false,
            failed: false,
        })
        try {
            await link(key)
            this.setState({
                linking: false,
                success: true,
                failed: false,
            })
        } catch (e) {
            this.setState({
                linking: false,
                success: false,
                failed: true,
            })
        }
    }

    render() {
        const style = {
            backgroundColor: "#0088cc",
            color: 'white'
        }

        if (this.state.linking) {
            return <Spinner />
        }

        return (
            <div>
                <Hero style={style} dark>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                Telegram
                            </Title>
                            <SubTitle>
                                Your awesome Telegram bot for Makerlog. Add done tasks from your messenger and check your stats.
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <Container>
                    <br />
                    {this.state.success &&
                        <Message success>
                            <Message.Body>Linked successfully.</Message.Body>
                        </Message>
                    }
                    {this.state.failed &&
                        <Message danger>
                            <Message.Body>Failed to link.</Message.Body>
                        </Message>
                    }
                    <Card>
                        <Card.Content>
                            <center>
                                <SubTitle is="5" className={"has-text-grey"}>Click the button below to get started.</SubTitle>
                                <OutboundLink className={"button is-medium is-info"} to={"http://telegram.me/MakerlogBot"}>
                                    Add to Telegram
                                </OutboundLink>
                                <hr />
                                <small>You can also <OutboundLink to={'https://t.me/makerlog'}>join the Makerlog groupchat</OutboundLink> to interact with other makers.</small>
                            </center>
                        </Card.Content>
                    </Card>
                </Container>
            </div>
        )
    }
}

Telegram.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Telegram);