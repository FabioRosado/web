import React from 'react';
import { Title, Media, SubTitle, Card as BulmaCard } from 'vendor/bulma';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import {withCurrentUser, LoggedOutOnly} from "features/users";
import {connect} from "react-redux";
import {mapStateToProps, mapDispatchToProps} from "ducks/apps";

let BaseCard = styled(BulmaCard)`
  & .title:not(.is-spaced) + .subtitle  {
    margin-top: -1.2rem !important;
  }
`

const TodoistCard = styled(BaseCard)`
  background-color: #F22613 !important;
  border-color: #F22613 !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const SignedInSection = styled.div`
  opacity: ${props => props.isLoggedIn ? '1' : '0.3'};
`

const InstallButton = ({ to, installed }) => (
    <Link to={to} className={"button is-rounded" + (installed ? ' is-outlined is-inverted is-primary' : '')}>
        {installed ? 'Settings' : 'Install'}
    </Link>
)

const SlackCard = styled(BaseCard)`
  background-color: #e6186d !important;
  border-color: #e6186d !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const TrelloCard = styled(BaseCard)`
  background-color: #0079BF !important;
  border-color: #0079BFs !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const GitHubCard = styled(BaseCard)`
  background-color: black !important;
  border-color: black !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const GitLabCard = styled(BaseCard)`
  background-color: #fc6d26 !important;
  border-color: #fc6d26 !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const NodeHostCard = styled(BaseCard)`
  background-color: #3498db !important;
  border-color: #3498db !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const WebhooksCard = styled(BaseCard)`
  background-color: #37985e !important;
  border-color: #37985e !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const ShipstreamsCard = styled(BaseCard)`
  background-color: #4B8BFF !important;
  border-color: #4B8BFF !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const TelegramCard = styled(BaseCard)`
  background-color: #0088cc !important;
  border-color: #0088cc !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const AppList = (props) => {
    return (
        <div>
            <LoggedOutOnly>
                <BaseCard>
                    <BaseCard.Content>
                        <center>
                            <Title className={"has-text-grey"} is={"4"}>
                                You must be signed in to use the following apps.
                            </Title>
                            <Link to={"/begin"} className={"button is-rounded is-primary"}>Get started</Link>
                        </center>
                    </BaseCard.Content>
                </BaseCard>
                <br />
            </LoggedOutOnly>

            <SignedInSection isLoggedIn={props.isLoggedIn}>
                <div className="tile is-ancestor">
                    <div className="tile is-parent">
                        <TodoistCard className="tile is-child">
                            <TodoistCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            Todoist
                                        </Title>
                                        <SubTitle is="6">
                                            Log done tasks straight from Todoist.
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/todoist'}
                                            installed={props.apps && props.apps['todoist'] ? props.apps['todoist'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <img src={"/assets/img/todoist.png"} width={80} alt={"Todoist"} />
                                    </Media.Right>
                                </Media>
                            </TodoistCard.Content>
                        </TodoistCard>
                    </div>
                    <div className="tile is-parent">
                        <SlackCard className="tile is-child">
                            <SlackCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            Slack
                                        </Title>
                                        <SubTitle is="6">
                                            Log tasks and see your stats with Makebot.
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/slack'}
                                            installed={props.apps && props.apps['slack'] ? props.apps['slack'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <img src={"/assets/img/slack-white.png"} width={80} alt={"Slack"} />
                                    </Media.Right>
                                </Media>
                            </SlackCard.Content>
                        </SlackCard>
                    </div>
                    <div className="tile is-parent">
                        <TrelloCard className="tile is-child">
                            <TrelloCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            Trello
                                        </Title>
                                        <SubTitle is="6">
                                            Log tasks from your Trello boards.
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/trello'}
                                            installed={props.apps && props.apps['trello'] ? props.apps['trello'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <FontAwesomeIcon icon={['fab', 'trello']} color={'white'} size={'5x'} />
                                    </Media.Right>
                                </Media>
                            </TrelloCard.Content>
                        </TrelloCard>
                    </div>
                </div>
                <div className="tile is-ancestor">
                    <div className="tile is-parent">
                        <GitHubCard className="tile is-child">
                            <GitHubCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            GitHub
                                        </Title>
                                        <SubTitle is="6">
                                            Log your daily commit counts from GitHub.
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/github'}
                                            installed={props.apps && props.apps['github'] ? props.apps['github'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <FontAwesomeIcon icon={['fab', 'github']} color={'white'} size={'5x'} />
                                    </Media.Right>
                                </Media>
                            </GitHubCard.Content>
                        </GitHubCard>
                    </div>
                    <div className="tile is-parent">
                        <GitLabCard className="tile is-child">
                            <GitLabCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            GitLab
                                        </Title>
                                        <SubTitle is="6">
                                            Log your daily commit counts from GitLab.
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/gitlab'}
                                            installed={props.apps && props.apps['gitlab'] ? props.apps['gitlab'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <FontAwesomeIcon icon={['fab', 'gitlab']} color={'white'} size={'5x'} />
                                    </Media.Right>
                                </Media>
                            </GitLabCard.Content>
                        </GitLabCard>
                    </div>
                    <div className="tile is-parent">
                        <NodeHostCard className="tile is-child">
                            <NodeHostCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            NodeHost
                                        </Title>
                                        <SubTitle is="6">
                                            Auto post tasks from NodeHost.
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/nodehost'}
                                            installed={props.apps && props.apps['nodehost'] ? props.apps['nodehost'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <FontAwesomeIcon icon={'rocket'} color={'white'} size={'5x'} />
                                    </Media.Right>
                                </Media>
                            </NodeHostCard.Content>
                        </NodeHostCard>
                    </div>
                </div>

                <div className="tile is-ancestor">
                    <div className="tile is-parent">
                        <WebhooksCard className="tile is-child">
                            <WebhooksCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            Webhooks
                                        </Title>
                                        <SubTitle is="6">
                                            Use webhooks to log from your apps, and make Makerlog truly yours.
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/webhooks'}
                                            installed={props.apps && props.apps['webhooks'] ? props.apps['webhooks'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <FontAwesomeIcon icon={'cogs'} color={'white'} size={'5x'} />
                                    </Media.Right>
                                </Media>
                            </WebhooksCard.Content>
                        </WebhooksCard>
                    </div>

                    <div className="tile is-parent">
                        <ShipstreamsCard className="tile is-child">
                            <ShipstreamsCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            Shipstreams
                                        </Title>
                                        <SubTitle is="6">
                                            Log when you go live and showcase your streams!
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/shipstreams'}
                                            installed={props.apps && props.apps['shipstreams'] ? props.apps['shipstreams'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <FontAwesomeIcon icon={'ship'} color={'white'} size={'5x'} />
                                    </Media.Right>
                                </Media>
                            </ShipstreamsCard.Content>
                        </ShipstreamsCard>
                    </div>
                    <div className="tile is-parent">
                        <TelegramCard className="tile is-child">
                            <TelegramCard.Content>
                                <Media>
                                    <Media.Content>
                                        <Title is="5">
                                            Telegram
                                        </Title>
                                        <SubTitle is="6">
                                            The official Makerlog bot & community
                                        </SubTitle>
                                        <InstallButton
                                            to={'/apps/telegram'}
                                            installed={props.apps && props.apps['telegram'] ? props.apps['telegram'].installed : false}
                                        />
                                    </Media.Content>
                                    <Media.Right>
                                        <FontAwesomeIcon icon={['fab', 'telegram']} color={'white'} size={'5x'} />
                                    </Media.Right>
                                </Media>
                            </TelegramCard.Content>
                        </TelegramCard>
                    </div>
                </div>
            </SignedInSection>
        </div>
    );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withCurrentUser(AppList));