import React, { Component } from 'react';
import Page from 'layouts/Page';
import { Card, Title, Button, Level, Hero as BaseHero, Container, SubTitle } from 'vendor/bulma';
import { connect } from 'react-redux';
import { actions as statsActions } from 'ducks/stats';
import { StatsLevel } from 'features/stats';
import { Bar as BarChart, Pie as PieChart } from 'react-chartjs-2';
import styled from 'styled-components';
import UserActivityGraph from '../../features/stats/components/UserActivityGraph';

const Hero = styled(BaseHero)`
    background-color: ${props => props.theme.primaryDarker} !important;
`

class StatsPage extends Component {
    render() {
        if (this.props.failed) return <Page>Failed to load stats. <br/> <Button onClick={this.props.loadStats}>Reload</Button></Page>;

        return (
            <Page contained={false} loading={!this.props.ready}>
                <Hero dark>
                    <Hero.Body>
                        <Container>
                            <Level>
                                <Level.Left>
                                    <Title is={3}>Stats</Title>
                                </Level.Left>
                                <Level.Right>
                                    <StatsLevel importantOnly />
                                </Level.Right>

                            </Level>
                        </Container>
                    </Hero.Body>
                </Hero>
                <div className="container" style={{paddingTop: 30}}>
                    <div className="columns">
                        <div className="column">
                            <SubTitle>At a glance</SubTitle>
                            <Card>
                                <Card.Content>
                                    <StatsLevel />
                                </Card.Content>
                            </Card>
                        </div>
                        <div className="column">
                            <SubTitle>Your activity</SubTitle>
                            <Card>
                                <Card.Content>
                                    {this.props.user &&  
                                       <UserActivityGraph user={this.props.user} />
                                    }
                                </Card.Content>
                            </Card>
                        </div>
                    </div>
                    <div className="columns">
                        <div className="column">
                            <SubTitle>This week reviewed</SubTitle>
                            <Card>
                                <Card.Content>
                                    {this.props.userStats.done_week &&
                                        <BarChart data={this.props.userStats.done_week} />
                                    }
                                </Card.Content>
                            </Card>
                        </div>

                        <div className="column">
                            <SubTitle>Work distribution</SubTitle>
                            <Card>
                                <Card.Content>
                                    {this.props.userStats.tasks_per_project &&
                                        <PieChart data={this.props.userStats.tasks_per_project} />
                                    }
                                </Card.Content>
                            </Card>
                        </div>
                    </div>
                </div>
            </Page>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ready: state.stats.ready,
        isLoading: state.stats.isLoading,
        failed: state.stats.failed,
        user: state.user.me,
        userStats: state.stats.user,
        worldStats: state.stats.world,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadStats: () => dispatch(statsActions.fetchStats())
    }
}

StatsPage.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StatsPage);